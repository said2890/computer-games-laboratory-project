﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace PlatformerGame
{
    class GameObject : RigidBody, Observer
       {
        //protected Model model;
        //protected Matrix[] transforms;

        //protected Vector3 position;
        //protected Vector3 rotation;
        //protected Vector3 scaling_factor;
        public String n = "A";

        protected Vector3 point = Vector3.Zero;
        protected Vector3 normal = Vector3.Up;

        private bool isSelected;
        private Vector3 lightDirection;
        private Vector3 diffuseColor;
        private Vector3 specularColor;

        private BoundingBox bbox;


        #region Properties

        public Vector3 Point
        {
            get { return this.point; }
            set { this.point = value; }
        }

        public Vector3 Normal
        {
            get { return this.normal; }
            set { this.normal = value; }
        }

        #endregion

        public GameObject() : base()
        {
            this.isSelected = false;
            this.lightDirection = new Vector3(0, -1, 0);
            this.diffuseColor = new Vector3(0.5f, 0.7f, 0);
            this.specularColor = new Vector3(0, 1, 0);
            init();
            this.isStatic = false;
            this.isSpherical = false;
        }

        //public Vector3 Position { set { this.position = value; } get { return this.position; } }
        //public Vector3 Scaling { set { this.scaling_factor = value; } get { return this.scaling_factor; } }
        //public Vector3 Rotation { set { this.rotation = value; } get { return this.rotation; } }
        public bool IsSelected { set { this.isSelected = value; } get { return this.isSelected; } }

        //Lighting
        public Vector3 LightDirection { set { this.lightDirection = value; } get { return this.lightDirection; } }
        public Vector3 SpecularColor { set { this.specularColor = value; } get { return this.specularColor; } }
        public Vector3 DiffuseColor { set { this.diffuseColor = value; } get { return this.diffuseColor; } }
       

        public Vector4 getSphere() { return new Vector4(this.model.Meshes[0].BoundingSphere.Center, this.model.Meshes[0].BoundingSphere.Radius); }


        
        public virtual void loadModel(Model model)
        {
            this.model = model;
            //this.objectModel = model;

            if (this.model != null)
            {
                this.transforms = new Matrix[this.model.Bones.Count];
                
                this.model.CopyAbsoluteBoneTransformsTo(this.transforms);               
            }       
        }

        public virtual void init()
        {
            this.position = new Vector3(0, 0, 0);
            this.rotation = new Vector3(0, 0, 0);
            this.scaling_factor = new Vector3(1, 1, 1);
        }

    

        public virtual void update(GraphicsDevice graphics, GameTime gameTime)
        {
            if(this.isSelected == false)
            base.update((float)gameTime.ElapsedGameTime.TotalSeconds);     
        }

   

        public virtual void draw(Camera2 camera, bool checkBone)
        {
            
            foreach (ModelMesh mesh in this.model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                
                    effect.LightingEnabled = true;
                    effect.SpecularPower = 0.1f;
                    effect.DirectionalLight0.DiffuseColor = this.diffuseColor; 
                    effect.DirectionalLight0.Direction = this.lightDirection;
                    effect.DirectionalLight0.SpecularColor = this.specularColor; 

                    if (checkBone == true) 
                        //effect.World = transforms[mesh.ParentBone.Index] * Matrix.CreateRotationX(MathHelper.ToRadians(this.rotation.X)) * Matrix.CreateRotationY(MathHelper.ToRadians(this.rotation.Y)) * Matrix.CreateRotationZ(MathHelper.ToRadians(this.rotation.Z)) * Matrix.CreateScale(this.scaling_factor) * Matrix.CreateTranslation(this.position);
                        effect.World = transforms[mesh.ParentBone.Index] * base.Transformation;
                    else 
                        //effect.World =  Matrix.CreateRotationX(MathHelper.ToRadians(this.rotation.X)) * Matrix.CreateRotationY(MathHelper.ToRadians(this.rotation.Y)) * Matrix.CreateRotationZ(MathHelper.ToRadians(this.rotation.Z)) * Matrix.CreateScale(this.scaling_factor) * Matrix.CreateTranslation(this.position);
                        effect.World = base.Transformation;
                    effect.View = camera.getView();
                    effect.Projection = camera.getProjection();
                }
                mesh.Draw();
            }

           

        }

        public void debugHandleInput(ref KeyboardState currentKeyboardState)
        {
            if (this.IsSelected == true)
            {

                if (currentKeyboardState.IsKeyDown(Keys.W))
                {
                    this.position.Z += 10.0f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.S))
                {
                    this.position.Z -= 10.0f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.A))
                {
                    this.position.X += 10.0f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.D))
                {
                    this.position.X -= 10.0f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.Q))
                {
                    this.rotation.Y += 10.0f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.E))
                {
                    this.rotation.Y -= 10.0f; return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.R))
                {
                    //reset
                    this.position = new Vector3(0, 0, 0);
                    this.rotation = new Vector3(0, 0, 0);
                    this.scaling_factor = new Vector3(1, 1, 1); return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.T))
                {
                    this.position.Y += 10.0f; return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.G))
                {
                    this.position.Y -= 10.0f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.Tab))
                {
                    Console.WriteLine("Position : " + this.position);
                    Console.WriteLine("Rotation : " + this.rotation);
                    Console.WriteLine("Scaleing : " + this.scaling_factor);
                }

                if (currentKeyboardState.IsKeyDown(Keys.NumPad1))
                {
                    this.scaling_factor.X += 0.1f; return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.NumPad2))
                {
                    this.scaling_factor.Y += 0.1f; return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.NumPad3))
                {
                    this.scaling_factor.Z += 0.1f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.Z))
                {
                    this.scaling_factor.X -= 0.1f; return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.X))
                {
                    this.scaling_factor.Y -= 0.1f; return;
                }
                if (currentKeyboardState.IsKeyDown(Keys.C))
                {
                    this.scaling_factor.Z -= 0.1f; return;
                }

                if (currentKeyboardState.IsKeyDown(Keys.U))
                {
                    Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
                    Vector3 v = new Vector3(0, 0, 120f / 60f);
                    v = Vector3.Transform(v, forwardMovement);
                    this.position.Z += v.Z;
                    this.position.X += v.X;
                    this.position.Y += v.Y;
                }

                if (currentKeyboardState.IsKeyDown(Keys.H))
                {
                    Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw + 1.5f);
                    Vector3 v = new Vector3(0, 0, (120f / 60f));
                    v = Vector3.Transform(v, forwardMovement);
                    this.position.Z += v.Z;
                    this.position.X += v.X;
                    this.position.Y += v.Y;
                }

                if (currentKeyboardState.IsKeyDown(Keys.J))
                {
                    Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
                    Vector3 v = new Vector3(0, 0, -(120f / 60f));
                    v = Vector3.Transform(v, forwardMovement);
                    this.position.Z += v.Z;
                    this.position.X += v.X;
                    this.position.Y += v.Y;
                }

                if (currentKeyboardState.IsKeyDown(Keys.K))
                {
                    Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw - 1.5f);
                    Vector3 v = new Vector3(0, 0, (120f / 60f));
                    v = Vector3.Transform(v, forwardMovement);
                    this.position.Z += v.Z;
                    this.position.X += v.X;
                    this.position.Y += v.Y;
                }
            }
        }

        public void moveX(float move_amount)
        {
            Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw - 1.5f);
            Vector3 v = new Vector3(0, 0, move_amount);
            v = Vector3.Transform(v, forwardMovement);
            this.position.Z += v.Z;
            this.position.X += v.X;
            this.position.Y += v.Y;
        }

        public void moveZ(float move_amount)
        {
            Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
            Vector3 v = new Vector3(0, 0, move_amount);
            v = Vector3.Transform(v, forwardMovement);
            this.position.Z += v.Z;
            this.position.X += v.X;
            this.position.Y += v.Y;
        }

        public void moveY(float move_amount)
        {

            this.position.Y += move_amount;
        }

        public void checkNotif(String g)
        {

        }
    }
}
