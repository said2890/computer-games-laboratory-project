﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace PlatformerGame
{
    /// <summary>
    /// Singleton class to detect collisions. Use Instance property.
    /// </summary>
    class CollisionDetector
    {
        #region Fields

        private static readonly CollisionDetector instance = new CollisionDetector();

        #endregion

        #region Constructors

        private CollisionDetector() { }

        #endregion

        #region Properties

        public static CollisionDetector Instance
        {
            get { return instance; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks if two balls are intersecting. NOT COMPLETED. ADD RESPONSE FOR THE SECOND BALL
        /// </summary>
        /// <param name="ball"></param>
        /// <param name="plane"></param>
        
        public void Detect(ref SphericalObject ball1, ref SphericalObject ball2)
        {


            BoundingSphere ballSphere1 = ball1.createBoundingSphere(ball1.Scaling);
            BoundingSphere ballSphere2 = ball2.createBoundingSphere(ball2.Scaling);

            if (ballSphere2.Intersects(ballSphere1))
            {
                Vector3 distanceVector = ballSphere1.Center - ballSphere2.Center;
                Vector3 contactNormal = Vector3.Normalize(distanceVector);
                Vector3 penetration = contactNormal * (ballSphere1.Radius + ballSphere2.Radius - distanceVector.Length());

                ball1.Position += penetration;
                //ball2.Position -= penetration;

                Vector3 Vab = ball1.Velocity - ball2.Velocity;
                Vector3 r1 = Vector3.Normalize(ball1.Velocity);
                Vector3 r2 = Vector3.Normalize(ball2.Velocity);

                float f = Vector3.Dot(-(1 + ball1.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1/ball1.Mass + 1/ball2.Mass));
                ball1.Velocity = ball1.Velocity + f/ball1.Mass * contactNormal;
                ball2.Velocity = ball2.Velocity - f/ball2.Mass * contactNormal;

                //ball1.Velocity = ball1.Velocity + f / ball1.Mass * (Vector3.Dot(contactNormal, r1)) * r1;
                //ball2.Velocity = ball2.Velocity - f / ball2.Mass * (Vector3.Dot(contactNormal, r2)) * r2;
            }

        }

        //public void Detect(ref GameObject go1, ref GameObject go2)
        //{
        //    if (go1.createBoundingBox(go1.Scaling).Intersects(go2.createBoundingBox(go2.Scaling)))
        //    {
        //        //
        //    }
        //}

        /// <summary>
        /// Checks if the ball intersects with the plane.
        /// </summary>
        /// <param name="ball"></param>
        /// <param name="plane"></param>       
        public void Detect(ref SphericalObject ball, ref GameObject gObject)
        {
            BoundingBox planeBoundingBox = gObject.getBoundingBox();
            BoundingOrientedBox planeBoundingOrientedBox = BoundingOrientedBox.CreateFromBoundingBox(planeBoundingBox);
            BoundingSphere ballSphere = ball.createBoundingSphere(ball.Scaling);


            if (ballSphere.Intersects(planeBoundingBox))
            {
                Vector3 closestPoint = Vector3.Clamp(ballSphere.Center, planeBoundingBox.Min, planeBoundingBox.Max);
                Vector3 distanceVector = ballSphere.Center - closestPoint;
                Vector3 contactNormal = Vector3.Normalize(distanceVector);
                Vector3 penetration = contactNormal * (ballSphere.Radius - distanceVector.Length());

                //float weight1 = ball.Mass / (ball.Mass + gObject.Mass);
                //float weight2 = gObject.Mass / (ball.Mass + gObject.Mass);

                ball.Position += penetration; Console.WriteLine(penetration);

                //Vector3 totalVelocity = ball.Velocity * weight1 + gObject.Velocity * weight2;

                Vector3 Vab = ball.Velocity - gObject.Velocity;
                Vector3 r1 = Vector3.Normalize(ball.Velocity);
                Vector3 r2 = Vector3.Normalize(gObject.Velocity);

                float f1 = Vector3.Dot(-(1 + ball.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / ball.Mass + 1 / gObject.Mass));
                float f2 = Vector3.Dot(-(1 + gObject.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / ball.Mass + 1 / gObject.Mass));
                ball.Velocity = (ball.Velocity + f1 / ball.Mass * contactNormal) * gObject.Friction;
                gObject.Velocity = gObject.Velocity - f2 / gObject.Mass * contactNormal;

                //ball.Velocity = (ball.Velocity - 2 * (Vector3.Dot(contactNormal, ball.Velocity)) * contactNormal) * ball.Restitution;
            }
        }

        /// <summary>
        /// Checks if the ball intersects with the plane.
        /// </summary>
        /// <param name="ball"></param>
        /// <param name="plane"></param>       
        public void Detect(ref GameObject gObject, ref SphericalObject ball)
        {
            BoundingBox planeBoundingBox = gObject.getBoundingBox();
            BoundingOrientedBox planeBoundingOrientedBox = BoundingOrientedBox.CreateFromBoundingBox(planeBoundingBox);
            BoundingSphere ballSphere = ball.createBoundingSphere(ball.Scaling);


            if (ballSphere.Intersects(planeBoundingBox))
            {
                Vector3 closestPoint = Vector3.Clamp(ballSphere.Center, planeBoundingBox.Min, planeBoundingBox.Max);
                Vector3 distanceVector = ballSphere.Center - closestPoint;
                Vector3 contactNormal = Vector3.Normalize(distanceVector);
                Vector3 penetration = contactNormal * (ballSphere.Radius - distanceVector.Length());

                //float weight1 = ball.Mass / (ball.Mass + gObject.Mass);
                //float weight2 = gObject.Mass / (ball.Mass + gObject.Mass);

                ball.Position += penetration; Console.WriteLine(penetration);

                //Vector3 totalVelocity = ball.Velocity * weight1 + gObject.Velocity * weight2;

                Vector3 Vab = ball.Velocity - gObject.Velocity;
                Vector3 r1 = Vector3.Normalize(ball.Velocity);
                Vector3 r2 = Vector3.Normalize(gObject.Velocity);

                float f1 = Vector3.Dot(-(1 + ball.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / ball.Mass + 1 / gObject.Mass));
                float f2 = Vector3.Dot(-(1 + gObject.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / ball.Mass + 1 / gObject.Mass));
                ball.Velocity = (ball.Velocity + f1 / ball.Mass * contactNormal) * gObject.Friction;
                gObject.Velocity = gObject.Velocity - f2 / gObject.Mass * contactNormal;

                //ball.Velocity = (ball.Velocity - 2 * (Vector3.Dot(contactNormal, ball.Velocity)) * contactNormal) * ball.Restitution;
            }
        }

        /// <summary>
        /// Checks if a plane intersects with another plane. INCOMPLETE
        /// </summary>
        /// <param name="ball"></param>
        /// <param name="plane"></param>

        public void Detect(ref GameObject gObject1, ref GameObject gObject2)
        {

            if (!gObject1.IsSpherical && gObject2.IsSpherical)
            {
                BoundingBox planeBoundingBox = gObject1.getBoundingBox();
                BoundingOrientedBox planeBoundingOrientedBox = BoundingOrientedBox.CreateFromBoundingBox(planeBoundingBox);
                BoundingSphere ballSphere = gObject2.createBoundingSphere(gObject2.Scaling);


                if (ballSphere.Intersects(planeBoundingBox))
                {
                    Vector3 closestPoint = Vector3.Clamp(ballSphere.Center, planeBoundingBox.Min, planeBoundingBox.Max);
                    Vector3 distanceVector = ballSphere.Center - closestPoint;
                    Vector3 contactNormal = Vector3.Normalize(distanceVector);
                    Vector3 penetration = contactNormal * (ballSphere.Radius - distanceVector.Length());

                    //float weight1 = ball.Mass / (ball.Mass + gObject.Mass);
                    //float weight2 = gObject.Mass / (ball.Mass + gObject.Mass);

                    gObject2.Position += penetration; //Console.WriteLine(penetration);

                    //Vector3 totalVelocity = ball.Velocity * weight1 + gObject.Velocity * weight2;

                    Vector3 Vab = gObject2.Velocity - gObject1.Velocity;
                    Vector3 r1 = Vector3.Normalize(gObject2.Velocity);
                    Vector3 r2 = Vector3.Normalize(gObject1.Velocity);

                    float f1 = Vector3.Dot(-(1 + gObject2.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject2.Mass + 1 / gObject1.Mass));
                    float f2 = Vector3.Dot(-(1 + gObject1.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject2.Mass + 1 / gObject1.Mass));
                    gObject2.Velocity = (gObject2.Velocity + f1 / gObject2.Mass * contactNormal) * gObject1.Friction;
                    gObject1.Velocity = gObject1.Velocity - f2 / gObject1.Mass * contactNormal;

                    //ball.Velocity = (ball.Velocity - 2 * (Vector3.Dot(contactNormal, ball.Velocity)) * contactNormal) * ball.Restitution;
                }

            }
            else if (gObject1.IsSpherical && !gObject2.IsSpherical)
            {
               
                BoundingBox planeBoundingBox = gObject2.getBoundingBox();
                BoundingOrientedBox planeBoundingOrientedBox = BoundingOrientedBox.CreateFromBoundingBox(planeBoundingBox);
                BoundingSphere ballSphere = gObject1.createBoundingSphere(gObject1.Scaling);


                if (ballSphere.Intersects(planeBoundingBox))
                {
                    Vector3 closestPoint = Vector3.Clamp(ballSphere.Center, planeBoundingBox.Min, planeBoundingBox.Max);
                    Vector3 distanceVector1 = ballSphere.Center - closestPoint;
                    Vector3 distanceVector2 = closestPoint - ballSphere.Center;
                    Vector3 contactNormal1 = Vector3.Normalize(distanceVector1);
                    Vector3 contactNormal2 = Vector3.Normalize(distanceVector2);
                   
                    Vector3 penetration1 = contactNormal1 * (ballSphere.Radius - distanceVector1.Length());
                    Vector3 penetration2 = contactNormal2 * (ballSphere.Radius - distanceVector2.Length());
                    //penetration2 *= 1.3f;
                    //float weight1 = ball.Mass / (ball.Mass + gObject.Mass);
                    //float weight2 = gObject.Mass / (ball.Mass + gObject.Mass);

                    gObject1.Position += penetration1; //Console.WriteLine(penetration);
                    if(!gObject2.IsStatic)
                    gObject2.Position += penetration2;

                    //Vector3 totalVelocity = ball.Velocity * weight1 + gObject.Velocity * weight2;

                    Vector3 Vab1 = gObject1.Velocity - gObject2.Velocity;
                    Vector3 Vab2 = gObject2.Velocity - gObject1.Velocity;
                    Vector3 r1 = Vector3.Normalize(gObject1.Velocity);
                    Vector3 r2 = Vector3.Normalize(gObject2.Velocity);

                    float f1 = Vector3.Dot(-(1 + gObject1.Restitution) * Vab1, contactNormal1) / Vector3.Dot(contactNormal1, contactNormal1 * (1 / gObject1.Mass + 1 / gObject2.Mass));
                    float f2 = Vector3.Dot(-(1 + gObject2.Restitution) * Vab1, contactNormal2) / Vector3.Dot(contactNormal2, contactNormal2 * (1 / gObject1.Mass + 1 / gObject2.Mass));
                    gObject1.Velocity = (gObject1.Velocity + f1 / gObject1.Mass * contactNormal1) * gObject2.Friction;
                    gObject2.Velocity = gObject2.Velocity - f2 / gObject2.Mass * contactNormal2;
                    
                    //ball.Velocity = (ball.Velocity - 2 * (Vector3.Dot(contactNormal, ball.Velocity)) * contactNormal) * ball.Restitution;
                }
            }
            else if (gObject1.IsSpherical && gObject2.IsSpherical)
            {

                BoundingSphere ballSphere1 = gObject1.createBoundingSphere(gObject1.Scaling);
                BoundingSphere ballSphere2 = gObject2.createBoundingSphere(gObject2.Scaling);

                if (ballSphere2.Intersects(ballSphere1))
                {
                    Vector3 distanceVector = ballSphere1.Center - ballSphere2.Center;
                    Vector3 contactNormal = Vector3.Normalize(distanceVector);
                    Vector3 penetration = contactNormal * (ballSphere1.Radius + ballSphere2.Radius - distanceVector.Length());

                    gObject1.Position += penetration *1.1f;
                    gObject2.Position -= penetration;
                    //ball2.Position -= penetration;

                    Vector3 Vab = gObject1.Velocity - gObject2.Velocity;
                    Vector3 r1 = Vector3.Normalize(gObject1.Velocity);
                    Vector3 r2 = Vector3.Normalize(gObject2.Velocity);

                    float f = Vector3.Dot(-(1 + gObject1.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject1.Mass + 1 / gObject2.Mass));
                    gObject1.Velocity = gObject1.Velocity + f / gObject1.Mass * contactNormal;
                    gObject2.Velocity = gObject2.Velocity - f / gObject2.Mass * contactNormal;

                    //ball1.Velocity = ball1.Velocity + f / ball1.Mass * (Vector3.Dot(contactNormal, r1)) * r1;
                    //ball2.Velocity = ball2.Velocity - f / ball2.Mass * (Vector3.Dot(contactNormal, r2)) * r2;
                }
            }
            else if (!gObject1.IsSpherical && !gObject2.IsSpherical)
            {
               // if (gObject1.n.Equals("THE BOX")) Console.WriteLine(" both not shpere");
               // if (gObject2.n.Equals("THE BOX")) Console.WriteLine(" both not shpere");
                //if (gObject1.IsStatic) { return; }

                BoundingBox boxWithBoxBB1 = gObject1.getBoundingBox();
                BoundingBox boxWithBoxBB2 = gObject2.getBoundingBox();

                if (boxWithBoxBB1.Intersects(boxWithBoxBB2))
                {
                    BoundingOrientedBox boxWithBoxBOB1 = BoundingOrientedBox.CreateFromBoundingBox(boxWithBoxBB1);
                    BoundingOrientedBox boxWithBoxBOB2 = BoundingOrientedBox.CreateFromBoundingBox(boxWithBoxBB2);


                    // Finding closest point to the center of the first plane
                    Vector3 closestPointToBox1 = Vector3.Clamp(boxWithBoxBOB1.Center, boxWithBoxBB2.Min, boxWithBoxBB2.Max);
                        // Finding closest point to the center of the second plane
                    Vector3 closestPointToBox2 = Vector3.Clamp(boxWithBoxBOB2.Center, boxWithBoxBB1.Min, boxWithBoxBB1.Max);
                    
                    if (!gObject1.IsStatic && gObject2.IsStatic)
                    {
                        Vector3 contactNormal = Vector3.Normalize(closestPointToBox1 - boxWithBoxBOB1.Center);
                        Vector3 penetration = contactNormal * (closestPointToBox2 - closestPointToBox1);

                        if (float.IsNaN(penetration.X) || float.IsNaN(penetration.Y) || float.IsNaN(penetration.Z))
                            return;

                        gObject1.Position += penetration;

                        Vector3 Vab = gObject1.Velocity;

                        float f = Vector3.Dot(-(1 + gObject1.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject1.Mass + 1 / gObject2.Mass));
                        gObject1.Velocity = (gObject1.Velocity + f / gObject1.Mass * contactNormal) * gObject2.Friction;
                        gObject2.Velocity = Vector3.Zero;

                    }

                    else if (!gObject2.IsStatic && gObject1.IsStatic)
                    {
                        Vector3 contactNormal = Vector3.Normalize(closestPointToBox2 - boxWithBoxBOB2.Center);
                        Vector3 penetration = contactNormal * (closestPointToBox2 - closestPointToBox1);

                        if (float.IsNaN(penetration.X) || float.IsNaN(penetration.Y) || float.IsNaN(penetration.Z))
                            return;

                        gObject2.Position += penetration;
                        Vector3 Vab = gObject2.Velocity;

                        float f = Vector3.Dot(-(1 + gObject2.Restitution) * Vab, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject1.Mass + 1 / gObject2.Mass));
                        gObject1.Velocity = Vector3.Zero;
                        gObject2.Velocity = (gObject2.Velocity + f / gObject2.Mass * contactNormal) * gObject1.Friction;

                    }


                    else if (!gObject1.IsStatic && !gObject2.IsStatic)
                    {
                        Vector3 contactNormal;
                        if (gObject1.Velocity == Vector3.Zero) contactNormal = Vector3.Normalize(gObject2.Velocity);
                        else if (gObject2.Velocity == Vector3.Zero) contactNormal = Vector3.Normalize(gObject1.Velocity);
                        else contactNormal = Vector3.Normalize(closestPointToBox1 - boxWithBoxBOB1.Center);

                        Vector3 penetration1 = -contactNormal * (closestPointToBox2 - closestPointToBox1) * 1.1f;
                        Vector3 penetration2 = -penetration1;

                        if (float.IsNaN(penetration1.X) || float.IsNaN(penetration1.Y) || float.IsNaN(penetration1.Z))
                            return;  //Console.WriteLine("Maaan");
                        if (gObject1.Position.Y > gObject2.Position.Y)
                            gObject1.Position += penetration1;

                        else 
                            gObject2.Position += penetration2;

                        Vector3 Vab1 = gObject1.Velocity - gObject2.Velocity;
                        //Vector3 Vab2 = gObject2.Vty - gObject1.Velocity;

                        float f1 = Vector3.Dot(-(1 + gObject1.Restitution) * Vab1, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject1.Mass + 1 / gObject2.Mass));
                        float f2 = Vector3.Dot(-(1 + gObject2.Restitution) * Vab1, contactNormal) / Vector3.Dot(contactNormal, contactNormal * (1 / gObject1.Mass + 1 / gObject2.Mass));
                        gObject1.Velocity = (gObject1.Velocity + f1 / gObject1.Mass * contactNormal);
                        gObject2.Velocity = (gObject2.Velocity - f2 / gObject2.Mass * contactNormal);

                        
                    }
                    
                    
                    
                    


                   

                    //gObject1.Velocity = (gObject1.Velocity - 2 * (Vector3.Dot(contactNormal1, gObject1.Velocity)) * contactNormal1) * gObject1.Restitution;
                    //gObject2.Velocity = (gObject2.Velocity - 2 * (Vector3.Dot(contactNormal1, gObject2.Velocity)) * contactNormal1) * gObject2.Restitution;
                }
            }
        }


        public void calculateVelocities(ref GameObject object1, GameObject object2)
        {
            Vector3 relativepos = object2.Position - object1.Position;
            float distance = relativepos.Length();
            
            // Add epsilon to avoid NaN.
            distance += 0.000001f;

            Vector3 relativeUnit = relativepos * (1.0f / distance);
            
            float mass1 = object1.Mass;
            float mass2 = object2.Mass;

            float m_inv = 1.0f / (mass1 + mass2);
            float weight1 = mass1 * m_inv; 
            float weight2 = mass2 * m_inv; 

            Vector3 velocity1 = object1.Velocity;
            Vector3 velocity2 = object2.Velocity;

            Vector3 velocityTotal = velocity1 * weight1 + velocity2 * weight2;
            Vector3 i2 = (velocity2 - velocityTotal) * mass2;

            if (Vector3.Dot(i2, relativeUnit) < 0)
            {
                // i1+i2 == 0, approx
                Vector3 di = Vector3.Dot(i2, relativeUnit) * relativeUnit;
                object1.Velocity = (-i2) * (object1.Restitution + 1) / mass1 + velocityTotal;
                object2.Velocity = i2 * (object2.Restitution + 1) / mass2 + velocityTotal;
            }
        }

        /// <summary>
        /// Render debug for a box
        /// </summary>
        /// <param name="debugDraw"></param>

        public void DebugRenderBox(DebugDraw debugDraw, GameObject gObject)
        {
            debugDraw.DrawWireBox(gObject.getBoundingBox(), Color.Yellow);
        }


        /// <summary>
        /// Render debug animation for a sphere (ball)
        /// </summary>
        /// <param name="debugDraw"></param>

        public void DebugRenderSphere(DebugDraw debugDraw, GameObject ball)
        {
            debugDraw.DrawWireSphere(ball.createBoundingSphere(ball.Scaling), Color.Red);
        }


        /// <summary>
        // Given 2 spheres with velocity, mass and size, evaluate whether
        // a collision occured, and if so, excatly where, and move sphere 2
        // at the contact point with sphere 1, and generate new velocities.
        /// </summary>
        public void SphereCollisionImplicit(ref SphericalObject ball1, ref SphericalObject ball2)
        {
            const float K_ELASTIC = 0.75f;

            BoundingSphere sphere1 = ball1.createBoundingSphere(ball1.Scaling);
            BoundingSphere sphere2 = ball2.createBoundingSphere(ball2.Scaling);

            Vector3 relativepos = ball2.Position - ball1.Position;
            float distance = relativepos.Length();
            float radii = sphere1.Radius + sphere2.Radius;

            if (distance >= radii)
            {
                return; // No collision
            }

            // Add epsilon to avoid NaN.
            distance += 0.000001f;

            Vector3 relativeUnit = relativepos * (1.0f / distance);
            Vector3 penetration = relativeUnit * (radii - distance);

            // Adjust the spheres' relative positions
            float mass1 = ball1.Mass;
            float mass2 = ball2.Mass;

            float m_inv = 1.0f / (mass1 + mass2);
            float weight1 = mass1 * m_inv; // relative weight of sphere 1
            float weight2 = mass2 * m_inv; // relative weight of sphere 2. w1+w2==1.0

            ball1.Position -= weight2 * penetration;
            ball2.Position += weight1 * penetration;

            // Adjust the objects’ relative velocities, if they are
            // moving toward each other.
            //
            // Note that we're assuming no friction, or equivalently, no angular momentum.
            //
            // velocityTotal = velocity of v2 in v1 stationary ref. frame
            // get reference frame of common center of mass
            Vector3 velocity1 = ball1.Velocity;
            Vector3 velocity2 = ball2.Velocity;

            Vector3 velocityTotal = velocity1 * weight1 + velocity2 * weight2;
            Vector3 i2 = (velocity2 - velocityTotal) * mass2;

            if (Vector3.Dot(i2, relativeUnit) < 0)
            {
                // i1+i2 == 0, approx
                Vector3 di = Vector3.Dot(i2, relativeUnit) * relativeUnit;
                i2 -= di * (K_ELASTIC + 1);
                ball1.Velocity = (-i2) / mass1 + velocityTotal;
                ball2.Velocity = i2 / mass2 + velocityTotal;
            }
        }

        #endregion

    }
}
