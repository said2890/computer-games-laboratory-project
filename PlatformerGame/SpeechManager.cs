﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition;
using System.Text;
using System.Windows.Forms;


namespace PlatformerGame
{
    class SpeechManager 
    {
        private static SpeechManager instance = null;
        private KinectManager kinect;
        //private static 
        private SpeechRecognitionEngine recognizer;
        private Choices choices;
        private Grammar grammer;

        private SpeechManager(ref KinectManager kinectManager)
        {
            kinect = kinectManager;
             // Select a speech recognizer that supports English.
              RecognizerInfo info = null;
              foreach (RecognizerInfo ri in SpeechRecognitionEngine.InstalledRecognizers())
              {
                  Console.WriteLine(ri.Name);
                if (ri.Culture.TwoLetterISOLanguageName.Equals("en"))
                {
                  info = ri;
                  Console.WriteLine(info.Id);
                  break;
                }
              }

            this.recognizer = new SpeechRecognitionEngine(info);

             this.choices = new Choices();

          

             this.choices.Add(new string[] {"DRAW", "SHOW","MOVE", "STOP","DESELECT", "SELECT", "CREATE BALL", "CREATE CUBE", "CANCEL", "TURN RIGHT" , "TURN LEFT"});


            GrammarBuilder gb = this.choices.ToGrammarBuilder();

           // gb.Append(this.choices);

            gb.Culture = info.Culture;

            this.grammer = new Grammar(gb);

            this.grammer.Enabled = true;

             // Create and load a dictation grammar.
            recognizer.LoadGrammar(this.grammer);

            // Add a handler for the speech recognized event.
            recognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(callback_SpeechRecognized);
            recognizer.SpeechDetected += new EventHandler<SpeechDetectedEventArgs>(callback_SpeechDetected);
            recognizer.SpeechHypothesized += new EventHandler<SpeechHypothesizedEventArgs>(callback_SpeechHypothesized);
            recognizer.SpeechRecognitionRejected += new EventHandler<SpeechRecognitionRejectedEventArgs>(callback_SpeechRejected);

            recognizer.MaxAlternates = 0;
            // Configure input to the speech recognizer.
            recognizer.SetInputToDefaultAudioDevice();
            

            // Start asynchronous, continuous speech recognition.
            recognizer.RecognizeAsync(RecognizeMode.Multiple);
            
            
        }

        public static SpeechManager getInstance(ref KinectManager kinectManager)
        {
            if (instance == null)
            {
                instance = new SpeechManager(ref kinectManager);
            }

            return instance;
        }


        // Create a simple handler for the SpeechRecognized event.
        public void callback_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Confidence > 0.85)
            {

                //MessageBox.Show("Speech recognized: " + e.Result.Text);
                Console.WriteLine("Speech recognized: " + e.Result.Confidence + "  " + e.Result.Text.ToString());

                this.kinect.checkRecognizedSpeech(e.Result.Text.ToString());
            }
            
            
        }

        public void callback_SpeechDetected(object sender, SpeechDetectedEventArgs e)
        {
            Console.WriteLine("Speech detected: " + e.AudioPosition);
        }

        public void callback_SpeechHypothesized(object sender, SpeechHypothesizedEventArgs e)
        {
            Console.WriteLine("Speech hypothesized: " + e.Result.Text);
        }

        public void callback_SpeechRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            foreach(RecognizedWordUnit word in e.Result.Words){
                Console.WriteLine("Speech rejected: " + word);
            }
            
        }

        public void Dispose()
        {
            this.recognizer.Dispose();
        }



        

    }
}
