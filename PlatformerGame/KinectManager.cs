﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;
using System.IO;


namespace PlatformerGame
{
    class KinectManager : Subject
    {
        private static KinectManager instance = null;
        Game1 mGame;
        private KinectSensor mKinect;

       
       
        private Color[] mLatestColorData;
        private Texture2D mColorImage;
        private Texture2D mDepthImage;
        private DepthImagePixel[] depthPixels;
        private Skeleton[] skeletons;

        private List<Vector2> gestures;
      
       // private Gesture curGesture;
        private List<Observer> observers;
        private Subject iSubject;
        //Hands hands;
        KinectBody body;
        public Joint head;
        private Joint leftHand, rightHand;
        private GameObject selectedObject;

        Vector3 rightElbowPos;
        Vector3 leftElbowPos;
        Vector2 drawnCenter;
        Vector2 expectedCenter;
        Joint[] elbows;

        private Player player;

        Texture2D rTexture;
        Texture2D squareTemplate;
        Rectangle squareTemplateRectangle;
        Vector2 squareTemplatePosition;
        private List<Vector2> squaretest;

        private String KinectInputState;
        private List<Vector2> squareTestPoints;

        public bool selected, canceled, resize, creating, drawsquare;
        public float createTime = 0;

        public float storeTime = 0;


        public static KinectManager getInstance(Game1 game){
            if (instance == null)
            {
                instance = new KinectManager(game);
            }

            return instance;
        }

        private KinectManager(Game1 game)
        {
            mGame = game;
            observers = new List<Observer>();
            elbows = new Joint[2];
            gestures = new List<Vector2>();
            //iSubject = new Subject();
            drawnCenter = new Vector2();
            squaretest = new List<Vector2>();
            squareTestPoints = new List<Vector2>();
            selectedObject = null;
            this.KinectInputState = "FREE";
            
        }

        public void setPlayerRef(ref Player p){
            this.player = p;
        }

        public string InitKinect()
        {
            selected = false;
            resize = false;
            creating = false;
            drawsquare = false;
            if (KinectSensor.KinectSensors.Count == 0)
            {
                return "Error: No kinect sensors!";
            }

            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.mKinect = potentialSensor;
                    break;
                }
            }


            try
            {
                mKinect = KinectSensor.KinectSensors[0];
               // mKinect.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
               // mKinect.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
               //mKinect.SkeletonStream.
                mKinect.SkeletonStream.TrackingMode = SkeletonTrackingMode.Default;
                
                mKinect.SkeletonStream.Enable(new TransformSmoothParameters()
                {
                    Smoothing = 0.9f,
                    Correction = 0.6f,
                    Prediction = 0.05f,
                    JitterRadius = 0.3f,
                    MaxDeviationRadius = 0.04f
                    
                });
                mKinect.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(mKinect_SkeletonFrameReady);

               // mKinect.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(mKinect_ColorFrameReady);
               // mKinect.DepthFrameReady += new EventHandler<DepthImageFrameReadyEventArgs>(mKinect_DepthFrameReady);

                if (this.mKinect != null)
                {
                    this.mKinect.Start();
                   // this.hands = Hands.getInstance();
                    this.body = KinectBody.getInstance();
                }
              //  mColorImage = new Texture2D(mGame.GraphicsDevice, mKinect.ColorStream.FrameWidth, mKinect.ColorStream.FrameHeight);
              //  mDepthImage = new Texture2D(mGame.GraphicsDevice, mKinect.DepthStream.FrameWidth, mKinect.DepthStream.FrameHeight);
                //Debug.WriteLineIf(debugging, kinect.Status);
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e.ToString());
            }





            return "";
        }

    

        void mKinect_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            using (var skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame == null)
                    return;

                if (skeletons == null ||
                    skeletons.Length != skeletonFrame.SkeletonArrayLength)
                {
                    
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                }

                skeletonFrame.CopySkeletonDataTo(skeletons);
                
            }

            Skeleton closestSkeleton = skeletons.Where(s => s.TrackingState == SkeletonTrackingState.Tracked)
                                                .OrderBy(s => s.Position.Z * Math.Abs(s.Position.X))
                                                .FirstOrDefault();
           

                


            if (closestSkeleton == null)
                return;

            this.body.updateJoints(closestSkeleton);
        }

        

        //Inverse the positions

        public Vector2 getRightHandPos2D()
        {
            CoordinateMapper mapper = mKinect.CoordinateMapper;
            var point = mapper.MapSkeletonPointToColorPoint(body.RightHandJoint.Position, mKinect.ColorStream.Format);
            return new Vector2(point.X, point.Y);
        }


        //inverse the positions.
        public Vector2 getLeftHandPos2D()
        {
            CoordinateMapper mapper = mKinect.CoordinateMapper;
            var point = mapper.MapSkeletonPointToColorPoint(body.LeftHandJoint.Position, mKinect.ColorStream.Format);
            return new Vector2(point.X, point.Y);

        }

        //public DepthImagePoint getRightHandPos()
        //public Vector3 getRightHandPos(float scaleFactor)
        //{          
        //    return new Vector3(-1*hands.RightJoint.Position.X * scaleFactor, hands.RightJoint.Position.Y * scaleFactor, hands.RightJoint.Position.Z * scaleFactor);
        //}

        public Vector3 getJointPos(JointType t, Vector3 s)
        {
            return body.getPosition3D(t, s);
        }



        //public Vector3 getLeftHandPos(float scaleFactor)
        //{        
        //    return new Vector3(-1*hands.LeftJoint.Position.X * scaleFactor, hands.LeftJoint.Position.Y *scaleFactor , hands.LeftJoint.Position.Z * scaleFactor);
        //}

     

        private byte[] ConvertDepthFrame(short[] depthFrame, DepthImageStream depthStream)
        {
            int RedIndex = 0, GreenIndex = 1, BlueIndex = 2, AlphaIndex = 3;

            byte[] depthFrame32 = new byte[depthStream.FrameWidth * depthStream.FrameHeight * 4];

            for (int i16 = 0, i32 = 0; i16 < depthFrame.Length && i32 < depthFrame32.Length; i16++, i32 += 4)
            {
                int player = depthFrame[i16] & DepthImageFrame.PlayerIndexBitmask;
                int realDepth = depthFrame[i16] >> DepthImageFrame.PlayerIndexBitmaskWidth;

                // transform 13-bit depth information into an 8-bit intensity appropriate
                // for display (we disregard information in most significant bit)
                byte intensity = (byte)(~(realDepth >> 4));

                depthFrame32[i32 + RedIndex] = (byte)(intensity);
                depthFrame32[i32 + GreenIndex] = (byte)(intensity);
                depthFrame32[i32 + BlueIndex] = (byte)(intensity);
                depthFrame32[i32 + AlphaIndex] = 255;
            }

            return depthFrame32;
        }

        void mKinect_DepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
        {
            DepthImageFrame depthVideoFrame = e.OpenDepthImageFrame();

            if (depthVideoFrame != null)
            {
                //Debug.WriteLineIf(debugging, "Frame");
                //Create array for pixel data and copy it from the image frame
                short[] pixelData = new short[depthVideoFrame.PixelDataLength];
                depthVideoFrame.CopyPixelDataTo(pixelData);

                for (int i = 0; i < 10; i++)
                {
                    //Debug.WriteLineIf(debugging, pixelData[i]); 
                }

                // Convert the Depth Frame
                // Create a texture and assign the realigned pixels
                //
                mDepthImage = new Texture2D(mGame.GraphicsDevice, depthVideoFrame.Width, depthVideoFrame.Height);
                mDepthImage.SetData(ConvertDepthFrame(pixelData, mKinect.DepthStream));

              

            }
        }

        void mKinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            ColorImageFrame colorVideoFrame = e.OpenColorImageFrame();

            if (colorVideoFrame != null)
            {
                //Create array for pixel data and copy it from the image frame
                Byte[] pixelData = new Byte[colorVideoFrame.PixelDataLength];
                colorVideoFrame.CopyPixelDataTo(pixelData);

                //Convert RGBA to BGRA
                Byte[] bgraPixelData = new Byte[colorVideoFrame.PixelDataLength];
                for (int i = 0; i < pixelData.Length; i += 4)
                {
                    bgraPixelData[i] = pixelData[i + 2];
                    bgraPixelData[i + 1] = pixelData[i + 1];
                    bgraPixelData[i + 2] = pixelData[i];
                    bgraPixelData[i + 3] = (Byte)255; //The video comes with 0 alpha so it is transparent
                }

                // Create a texture and assign the realigned pixels
                mColorImage = new Texture2D(mGame.GraphicsDevice, colorVideoFrame.Width, colorVideoFrame.Height);
                mColorImage.SetData(bgraPixelData);
            }

        }

        public void DrawColorImage(SpriteBatch batch, GraphicsDevice device, Rectangle bounds)
        {

            if (mColorImage == null)
            {
                return;
            }

            // mColorImage = new Texture2D(device, 640, 480);

            // mColorImage.SetData<Color>(mLatestColorData);

            batch.Draw(mColorImage, bounds, Color.White);
        }

        

        public void DrawDepthImage(SpriteBatch batch, GraphicsDevice device, Rectangle bounds)
        {
            if (mDepthImage == null)
            {
                return;
            }

            batch.Draw(mDepthImage, bounds, Color.White);
        }

        public void setKinectSquareTemplate(Texture2D stemp, Rectangle rect)
        {
            this.squareTemplate = stemp;
            this.squareTemplateRectangle = rect;
        }

        public void loadKinectResources(GraphicsDevice gd)
        {
            this.rTexture = new Texture2D(gd, 1, 1);
            this.rTexture.SetData(new[] { Color.Chocolate });

            
            body.init(gd);
        }

        public void DisposeResources()
        {
            this.rTexture.Dispose();
            this.squareTemplate.Dispose();
            
            body.Dispose();
           // this.mColorImage.Dispose();
            //this.mDepthImage.Dispose();
        }

        public void draw(SpriteBatch sb)
        {
            

            

            //if (this.creating == true)
            //{
            //    //Vector2 c;
             
            //    foreach(Vector2 v in this.squaretest){


            //        sb.Draw(rTexture,
            //            new Rectangle((int)v.X, (int)v.Y, 16, 16),
            //            Color.Black);
            //        sb.Draw(rTexture,
            //            new Rectangle((int)center.X, (int)center.Y, 16, 16),
            //            Color.Orange);
            //        //sb.Draw(rTexture, this.rightRect, this.rightColor);
            //    }
            //}

            if (KinectInputState == "DRAW")
            {
                //sb.Draw(this.squareTemplate, this.squareTemplateRectangle, Color.White);

                foreach (Vector2 v in this.squaretest)
                {
                    sb.Draw(rTexture,
                        new Rectangle((int)v.X, (int)v.Y, 8, 8),
                        Color.Black);
                    sb.Draw(rTexture,
                        new Rectangle((int)drawnCenter.X, (int)drawnCenter.Y, 16, 16),
                        Color.Orange);
                }
                    
            }

            body.drawHands(sb);
        }


        public void update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            storeTime += dt;
            if (storeTime > 1)
            {
                storeTime = 0;
                body.getPrevHands = true;

            }
            else
            {
                body.getPrevHands = false;
            }

            body.RightHandPosition2D = new Vector3(getRightHandPos2D(), 1);
            body.LeftHandPosition2D = new Vector3(getLeftHandPos2D(), 1);
           
            //gesture recognition

            //if(KinectInputState.Equals("SELECTED")){
            //    float scaling_dx = (1.0f) * (this.getLeftHandPos(1f).X - this.getRightHandPos(1f).X);
            //    this.selectedObject.Scaling += new Vector3(scaling_dx);
            //}

            //if (creating == true)
            //{
            //    createTime += dt;
            //    if (createTime < 12 )
            //    {

            //        gestures.Add(this.getRightHandPos2D());
                   
            //        Console.WriteLine("Time spent : " + createTime);
                    
            //        foreach (Vector2 v in this.gestures)
            //        {
            //            center.X += v.X;
            //            center.Y += v.Y;

            //        }
            //        center /= this.gestures.Count;

            //        if (createTime > 4)
            //        {

            //            Vector2 first = this.gestures.ElementAt(0);
            //            Vector2 last = this.gestures.Last();

            //            if (Vector2.Distance(first, last) < 10)
            //            {
            //                Console.WriteLine("Completed");
            //                Console.WriteLine("Center : " + center);

                           

            //               // squaretest.Add(this.gestures.First());

            //                int c = this.gestures.Count;
            //                int d = c / 4;
            //                int getindex = 0;
            //                foreach (Vector2 v in this.gestures)
            //                {
            //                    squaretest.Add(this.gestures.ElementAt(getindex));
            //                    Console.WriteLine(this.gestures.ElementAt(getindex));
            //                    if((getindex + d) >= this.gestures.Count) getindex = this.gestures.Count -1;
            //                    else getindex += d;
            //                }


                            
                            
            //                creating = false;
            //                createTime = 0;
            //                this.gestures = new List<Vector2>();
            //            }
            //        }
                    
            //    }
            //    else
            //    {
            //        creating = false;
                    
            //        createTime = 0;
            //        this.gestures = new List<Vector2>();


                    
            //    }

            switch(KinectInputState){
                case "MOVE":
                    if (body.LeftHandPos.Z > 1.2f && body.RightHandPos.Z > 1.2f)
                    {
                        if (body.LeftHandPos.X > body.HeadPos.X &&
                            body.RightHandPos.X - 0.1f > body.HeadPos.X &&
                            body.LeftHandPos.Y < body.HeadPos.Y &&
                            body.RightHandPos.Y < body.HeadPos.Y)
                        {
                            Camera2.Instance().TurnRight();
                            return;
                            //Console.WriteLine(getRightHandPos(1f));
                        }
                        else if (body.LeftHandPos.X + 0.1f < body.HeadPos.X &&
                            body.RightHandPos.X < body.HeadPos.X &&
                            body.LeftHandPos.Y < body.HeadPos.Y &&
                            body.RightHandPos.Y < body.HeadPos.Y)
                        {
                            Camera2.Instance().TurnLeft();
                            return;
                        }
                        else if (body.LeftHandPos.Y > body.HeadPos.Y &&
                                 body.RightHandPos.Y > body.HeadPos.Y)
                        {
                            Camera2.Instance().LookUp();
                        }
                        else if (body.LeftHandPos.Y < body.CenterHipPos.Y &&
                           body.RightHandPos.Y < body.CenterHipPos.Y)
                        {
                            Camera2.Instance().LookDown();
                        }
                    }
                    if (body.RightHandPos.Z < 1.0f && body.LeftHandPos.Z < 1.0f 
                        && (body.HeadPos.Z - body.RightHandPos.Z) > 0.3f)
                    {
                        player.MoveForward();
                        return;
                    }
                    break;
                

                case "SHOW":
                    Console.WriteLine("Head.X == " + body.HeadPos.X);
                    Console.WriteLine("HEad.Y == " + body.HeadPos.Y);
                    Console.WriteLine("LeftHand.X == " + body.LeftHandPos.X);
                    Console.WriteLine("RightHand.X == " + body.RightHandPos.X);
                    Console.WriteLine("RightHand.Y == " + body.RightHandPos.Y);
                    Console.WriteLine("CenterHip.Y == " + body.CenterHipPos.Y);
                    Console.WriteLine("CenterShoulder.Y == " + body.CenterShoulderPos.Y);
                    
                    Console.WriteLine("body.LeftHandPos.X < body.RightHandPos.X == " +
                        (body.LeftHandPos.X < body.RightHandPos.X));
                    Console.WriteLine("body.RightHandPos.Y > body.CenterHipPos.Y == " +
                        (body.RightHandPos.Y > body.CenterHipPos.Y));
                    Console.WriteLine("body.RightHandPos.Y < body.CenterShoulderPos.Y == " +
                        (body.RightHandPos.Y < body.CenterShoulderPos.Y));
                    Console.WriteLine("Math.Abs(body.RightHandPos.X - body.HeadPos.X) < 0.4 == " + 
                        (Math.Abs(body.RightHandPos.X - body.HeadPos.X) < 0.4));

                    break;

               

                case "DRAW":
                    Vector2 leftTop = new Vector2(this.squareTemplateRectangle.Left,this.squareTemplateRectangle.Top);
                    Vector2 rightBottom = new Vector2(this.squareTemplateRectangle.Right, this.squareTemplateRectangle.Bottom);

                    //foreach (Vector2 point in squaretest)
                    //{
                    //     //  + ( y2-y1 ) * ( y2-y1 )
                    //}

                    captureShapeData();
                    break;

                case "CREATE BOX":
                    checkBoxShape();
                    //checkBallShape();
                    break;

                case "CREATE BALL":

                    checkBallShape();
                    break;

                case "FREE":
                    resetShapeData();

                    break;

                case "SELECT":
                    mGame.CheckMouseClick();
                    break;

                case "SELECTED" :

                    if (this.selectedObject != null)
                    {
                        this.selectedObject.moveZ(2.0f);
                        
                    }
                    

                    break;

                    
                
                default:
                    break;

            }

            //}

            //if (hands.LeftHandPosition.Z < 0.5f)
            //{
            //    curGesture = new Gesture("CROSS_GESTURE", ref this.hands);
            //}

            //if (hands.LeftHandPosition.X > hands.RightHandPosition.X && leftElbowPos.X < rightElbowPos.X)
            //{
            //    Console.WriteLine("CROSS");
            //}



        }

        public String InputState { get { return this.KinectInputState; } }
        public void ResetInputState() { this.KinectInputState = "FREE"; }
        public void setSelectedObject(ref GameObject obj) { this.selectedObject = obj; this.KinectInputState = "SELECTED"; }

        

        public Vector3 getLeftHandDifference()
        {
            
            return body.LeftHandPosition2D - body.prevLeftHand;
        }

        public Vector3 getCenterShoulderDifference()
        {
            return body.CenterShoulderPos - body.prevCenterShoulder;
        }

        public void captureShapeData()
        {
            this.squaretest.Add(new Vector2(body.RightHandPosition2D.X, body.RightHandPosition2D.Y));
            foreach (Vector2 point in this.squaretest)
            {
                this.drawnCenter += point;
            }
            Console.WriteLine("COUNT == " + this.squaretest.Count);
            this.drawnCenter /= this.squaretest.Count;
        }

        public void checkBoxShape()
        {
            Vector2 topLeft = Vector2.One, bottomRight = Vector2.One;
            Vector2 bottomLeft, topRight;
            int c = 0;
            foreach (Vector2 v in this.squaretest)
            {
                if (c == 0) topLeft = v;

                if (bottomRight.X < v.X) bottomRight.X = v.X;
                if (bottomRight.Y < v.Y) bottomRight.Y = v.Y;

                if (topLeft.X > v.X) topLeft.X = v.X;
                if (topLeft.Y > v.Y) topLeft.Y = v.Y;
                c++;
            }
            
            topRight = new Vector2(bottomRight.X, topLeft.Y);
            bottomLeft = new Vector2(topLeft.X, bottomRight.Y);

            this.expectedCenter += (bottomRight + topLeft + topRight + bottomLeft);
            this.expectedCenter /= 4;

            Microsoft.Kinect.Vector4 distFromCenter = new Microsoft.Kinect.Vector4();
            distFromCenter.X = ((topLeft.X - this.expectedCenter.X) * (topLeft.X - this.expectedCenter.X) + (topLeft.Y - this.expectedCenter.Y) * (topLeft.Y - this.expectedCenter.Y));
            distFromCenter.Y = ((topRight.X - this.expectedCenter.X) * (topRight.X - this.expectedCenter.X) + (topRight.Y - this.expectedCenter.Y) * (topRight.Y - this.expectedCenter.Y));
            distFromCenter.Z = ((bottomLeft.X - this.expectedCenter.X) * (bottomLeft.X - this.expectedCenter.X) + (bottomLeft.Y - this.expectedCenter.Y) * (bottomLeft.Y - this.expectedCenter.Y));
            distFromCenter.W = ((bottomRight.X - this.expectedCenter.X) * (bottomRight.X - this.expectedCenter.X) + (bottomRight.Y - this.expectedCenter.Y) * (bottomRight.Y - this.expectedCenter.Y));

            float rightDist = ((topRight.X - bottomRight.X)*(topRight.X - bottomRight.X)+(topRight.Y - bottomRight.Y)*(topRight.Y-bottomRight.Y));
            float bottomDist = ((bottomLeft.X - bottomRight.X)*(bottomLeft.X - bottomRight.X)+(bottomLeft.Y- bottomRight.Y)*(bottomLeft.Y- bottomRight.Y));

            Console.WriteLine(" MIN : " + bottomRight + "   === MAX : " + topLeft);
            Console.WriteLine("DRAWN CENTER : " + this.drawnCenter);
            Console.WriteLine("CALCD CENTER : " + this.expectedCenter);
            Console.WriteLine("DISTANCE FFROM CALCD CENTER : " + distFromCenter.X + " , " + distFromCenter.Y + " , " + distFromCenter.Z + " , " + distFromCenter.W);

            Console.WriteLine("SQR Right Distance : " + rightDist);
            Console.WriteLine("SQR Bottom Distance : " + bottomDist);

            if (Math.Abs(rightDist - bottomDist) <= 7500)
            {
                mGame.createBoxFromDrawing();
                Console.WriteLine("creating box ");
            }
            this.resetShapeData();
        }

        public void checkBallShape()
        {
            List<Vector2> circleCheckPoints = new List<Vector2>();
            Vector2 min = Vector2.One, max = Vector2.One;
            int c = 0;
            foreach (Vector2 v in this.squaretest)
            {
                if (c == 0) min = v;
                if (max.X < v.X) max.X = v.X;
                if (max.Y < v.Y) max.Y = v.Y;

                if (min.X > v.X) min.X = v.X;
                if (min.Y > v.Y) min.Y = v.Y;
            }

            float Rx = max.X - min.X; float rx = Rx / 2;
            float Ry = max.Y - min.Y; float ry = Ry / 2;

            Vector2 expectedCenter = new Vector2(min.X + rx, min.Y + ry);

            //foreach (Vector2 v in this.squaretest)
            //{
            //    if (((v.X - this.drawnCenter.X) * (v.X - this.drawnCenter.X) + (v.Y - this.drawnCenter.Y) * (v.Y - this.drawnCenter.Y)) > (rx + ry) / 2)
            //    {
            //        return;
            //    }
            //}

            Console.WriteLine("BALL MAX : " + max);
            Console.WriteLine("BALL MIN : " + min);
            Console.WriteLine("DRAWN CNETER : " + drawnCenter);
            Console.WriteLine("EXPECTED CENTER : " + (min.X + rx) + "  " + (min.Y + ry));

            float distCenters = ((expectedCenter.X - drawnCenter.X) * (expectedCenter.X - drawnCenter.X) + (expectedCenter.Y - drawnCenter.Y) * (expectedCenter.Y - drawnCenter.Y));
            //@TODO FIX THRESHOLD 1000
            if (Math.Abs(distCenters) <= 4000)
            {
                mGame.createBallFromDrawing();
                Console.WriteLine("creating ball");
            }
            else
            {
                Console.WriteLine("DIST CENTERS : " + distCenters);
            }

            this.resetShapeData();

            
        }

        public void resetShapeData()
        {
            this.drawnCenter = Vector2.Zero;
            this.squaretest = new List<Vector2>();
            this.KinectInputState = "FREE";
        }

        //override
        public void attach(Observer observer){
            this.observers.Add(observer);
        }

        //override
        public void detach(Observer observer){
            this.observers.Remove(observer);
        }

        //override
        public void notify(){
            foreach (Observer observer in observers)
            {
                //observer.checkNotif(curGesture);
            }
        }

        public void checkRecognizedSpeech(String keyword)
        {
            Console.WriteLine("Kinect received :: " + keyword);
            if( keyword.Equals("CREATE CUBE")){
                this.creating = true;
                this.drawsquare = false;
                this.KinectInputState = "CREATE BOX";
                return;
            } else if (keyword.Equals("CREATE BALL") )
            {
                this.creating = true;
                this.drawsquare = false;
                this.KinectInputState = "CREATE BALL";
                return;
            }else if(keyword.Equals("CANCEL") || keyword.Equals("STOP")){
                this.creating = false;
                this.createTime = 0;
                this.gestures = new List<Vector2>();
                this.resetShapeData();
                this.KinectInputState = "FREE";
                this.selectedObject = null;
                //mGame.lastSelected = -1;
                return;
                //this.drawsquare = true;
            }
            else if (keyword.Equals("SELECT"))
            {
                this.KinectInputState = "SELECT";
                return;
            }
            else if (keyword.Equals("MOVE"))
            {
                this.KinectInputState = "MOVE";
                return;
            }
            else if (keyword.Equals("DESELECT"))
            {
                if (this.KinectInputState.Equals("SELECTED") || this.selectedObject != null)
                {
                    this.selectedObject = null;
                    this.selectedObject.IsSelected = false;
                    this.KinectInputState = "FREE";
                }
                return;
            }
            else if(keyword.Equals("DRAW")){
                this.KinectInputState = "DRAW";
                return;
        }
            this.KinectInputState = "FREE";
        }


      

    }
}
