﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PlatformerGame
{
    /// <summary>
    /// This class represents any rigit body
    /// </summary>
    class RigidBody
    {

        #region Fields

        protected Vector3 position = Vector3.Zero;
        protected Vector3 rotation = Vector3.Zero;
        protected Vector3 scaling_factor = Vector3.Zero;
        protected float friction_coefficient;
        protected float mass;
        protected float restitution=0;
        protected Vector3 acceleration = new Vector3();
        protected Vector3 velocity = Vector3.Zero;
        protected Vector3 angularVelocity = Vector3.Zero;
        protected Matrix[] transforms;
        protected Matrix transformation = Matrix.Identity;
        protected Model model;
        protected bool isStatic;
        protected bool isSpherical;
        bool transformationNeedsUpdate = true;
        private BoundingBox boundingBox;

        #endregion

        #region Properties

        public bool IsStatic { set { this.isStatic = value; } get { return this.isStatic; } }
        public bool IsSpherical { set { this.isSpherical = value; } get { return this.isSpherical; } }

        public Matrix Transformation
        {
            get {
                if (transformationNeedsUpdate)
                {
                    updateTransformation();
                }
                return transformation;
            }
        }

        public float Friction
        {
            get { return this.friction_coefficient; }
            set { this.friction_coefficient = value; }
        }

        public Vector3 Rotation
        {
            get { return this.rotation; }
            set 
            { 
                this.rotation = value;
                this.transformationNeedsUpdate = true;
            }
        }

        public Vector3 Position
        {
            get { return this.position;  }
            set { 
                this.position = value;
                this.transformationNeedsUpdate = true;
            }
        }

        public Vector3 Scaling 
        {
            get { return this.scaling_factor; } 
            set { 
                this.scaling_factor = value;
                this.transformationNeedsUpdate = true;
            } 
        }

        public float Mass
        {
            get { return this.mass; }
            set { this.mass = value; }
        }

        public float Restitution
        {
            get { return this.restitution; }
            set { this.restitution = value; }
        }

        public Vector3 Acceleration
        {
            get { return this.acceleration; }
            set { this.acceleration = value; }
        }

        public Vector3 Velocity
        {
            get { return this.velocity; }
            set { this.velocity = value; }
        }

        public Vector3 AngularVelocity
        {
            get { return this.angularVelocity; }
            set { this.angularVelocity = value; }
        }

        public Model Model
        {
            get { return model; }
            set { this.model = value; }
        }

        #endregion

        #region Methods


        /// <summary>
        /// This method calculates acceleration based on the applied force (Gravity, Throw etc.)
        /// </summary>
        /// <param name="force"></param>
        public void applyForce(Vector3 force)
        {
            acceleration = acceleration + force / mass;
        }

        /// <summary>
        /// Updates the position of a rigit body using Euler Integration
        /// </summary>
        /// <param name="time"></param>
        public void update(float time)
        {
            if(this.isStatic == false)
                this.Acceleration = new Vector3(0.0f, -98f, 0.0f);

           // updateTransformation();
            velocity = velocity + acceleration * time;
            position = position + velocity * time;

            //acceleration = Vector3.Zero;
            this.transformationNeedsUpdate = true;

        }

        /// <summary>
        /// Updates Transformation Matrix if a position of a rigid body has changed
        /// </summary>
        void updateTransformation()
        {
            if (this.transformationNeedsUpdate == false) return;

            this.transformation = Matrix.CreateRotationX(MathHelper.ToRadians(this.rotation.X)) * Matrix.CreateRotationY(MathHelper.ToRadians(this.rotation.Y)) *
                Matrix.CreateRotationZ(MathHelper.ToRadians(this.rotation.Z)) * Matrix.CreateScale(this.Scaling) * Matrix.CreateTranslation(this.Position);   
    
            this.transformationNeedsUpdate = false;
        }

        #endregion

        #region Collision
        

        ///// <summary>
        ///// Creates a bounding box around Model
        ///// </summary>
        ///// <returns>BoundingBox</returns>
        //public BoundingBox createBoundingBox()
        //{
        //    foreach (ModelMesh mesh in model.Meshes)
        //    {
        //        Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        //        Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

        //        foreach (ModelMeshPart meshPart in mesh.MeshParts)
        //        {
        //            int vertexStride = meshPart.VertexBuffer.VertexDeclaration.VertexStride;
        //            int vertexBufferSize = meshPart.NumVertices * vertexStride;

        //            float[] vertexData = new float[vertexBufferSize / sizeof(float)];
        //            meshPart.VertexBuffer.GetData<float>(vertexData);

        //            for (int i = 0; i < vertexBufferSize / sizeof(float); i += vertexStride / sizeof(float))
        //            {
        //                Vector3 transformedPosition = Vector3.Transform(new Vector3(vertexData[i], vertexData[i + 1], vertexData[i + 2]), this.transformation);

        //                min = Vector3.Min(min, transformedPosition);
        //                max = Vector3.Max(max, transformedPosition);
        //            }
        //        }
               
        //        return new BoundingBox(min, max);
        //    } return new BoundingBox(Vector3.Zero, Vector3.Zero);
        //}

        /// <summary>
        /// Creates a bounding box around Model
        /// </summary>
        /// <returns>BoundingBox</returns>
        public virtual BoundingBox getBoundingBox()
        {
            Vector3 extents = (this.boundingBox.Max - this.boundingBox.Min) / 2.0f;

            if (this.IsStatic) return this.boundingBox;
            else
            {
                this.boundingBox = new BoundingBox(this.position - extents, this.position + extents);
            }
            return this.boundingBox;
        }

        /// <summary>
        /// Creates a bounding box around Model
        /// </summary>
        /// <returns>BoundingBox</returns>
        public virtual void createBoundingBox()
        {

            foreach (ModelMesh mesh in model.Meshes)
            {

                Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);



                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    int vertexStride = meshPart.VertexBuffer.VertexDeclaration.VertexStride;
                    int vertexBufferSize = meshPart.NumVertices * vertexStride;

                    float[] vertexData = new float[vertexBufferSize / sizeof(float)];
                    meshPart.VertexBuffer.GetData<float>(vertexData);

                    for (int i = 0; i < vertexBufferSize / sizeof(float); i += vertexStride / sizeof(float))
                    {
                        // 1 inch = 2.54 cm. The model is created in inches, whereas the bounding box in cantimetres. Therefore multiply by 2.54 cm
                        Vector3 transformedPosition = Vector3.Transform(new Vector3(vertexData[i], vertexData[i + 1], vertexData[i + 2]), this.Transformation);
                        min = Vector3.Min(min, transformedPosition);
                        max = Vector3.Max(max, transformedPosition);
                    }
                }

                this.boundingBox = new BoundingBox(min, max);
                return;
            } this.boundingBox = new BoundingBox(Vector3.Zero, Vector3.Zero);
        }

        ///// <summary>
        ///// Creates a bounding sphere around Model
        ///// </summary>
        ///// <returns>BoundingSphere</returns>
        //public BoundingSphere createBoundingSphere() 
        //{
        //    BoundingSphere sphere = new BoundingSphere();

        //    foreach (ModelMesh mesh in this.model.Meshes)
        //    {
        //            sphere = BoundingSphere.CreateMerged(sphere, mesh.BoundingSphere);
        //    }

        //    sphere.Center = this.position;
        //   // sphere.Radius = 1.f;
        //    return sphere;
        //}

        /// <summary>
        /// Creates a bounding sphere around Model
        /// </summary>
        /// <returns>BoundingSphere</returns>
        public virtual BoundingSphere createBoundingSphere(Vector3 scaling_factor)
        {
            BoundingSphere sphere = new BoundingSphere();

            foreach (ModelMesh mesh in this.model.Meshes)
            {
                sphere = BoundingSphere.CreateMerged(sphere, mesh.BoundingSphere);
            }

            sphere.Center = this.position;
            // sphere.Radius = 1.f;

            // sphere scales uniformly in all axis. therefore, X (or Y or Z) will suffice
            sphere.Radius *= scaling_factor.X;
            
            return sphere;
        }


        #endregion
    }
}
