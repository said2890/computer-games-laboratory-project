﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace PlatformerGame
{
    class Plane:GameObject
    {
        #region Fields

        protected Vector3 point = Vector3.Zero;
        protected Vector3 normal = Vector3.Up;

        #endregion

        #region Properties

        public Vector3 Point
        {
            get { return this.point; }
            set { this.point = value; }
        }

        public Vector3 Normal
        {
            get { return this.normal; }
            set { this.normal = value; }
        }

        #endregion
    }
}
