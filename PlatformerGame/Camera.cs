﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PlatformerGame
{
    class Camera
    {
        GraphicsDeviceManager graphicsManager;
        Matrix projection;
        Matrix view;

        bool isOrthogonal;

        Vector3 target;
        Vector3 up;
        Vector3 center;
        Vector3 eye;

        float fov;
        float zNear;
        float zFar;

        float yaw;
        float pitch;
        float distance;

        public Camera(GraphicsDeviceManager graphics, Vector3 position)
        {
            this.graphicsManager = graphics;

            this.zNear = 1.0f;
            this.zFar = 10000.0f;
            this.fov = MathHelper.ToRadians(40.0f);

            this.up = new Vector3(0, 1, 0);
            this.center = new Vector3(0, 0, 0);
            this.eye = position;
            this.target = this.center;

            this.yaw = (float)Math.PI * 0.75F;
            this.pitch = MathHelper.PiOver4;
            this.distance = 200.0f;

            this.isOrthogonal = false;
        }

        public Camera(GraphicsDeviceManager graphics)
        {
            this.graphicsManager = graphics;

            this.zNear = 1.0f;
            this.zFar = 10000.0f;
            this.fov = MathHelper.ToRadians(60.0f);


            this.up = new Vector3(0, 1.0f, 0);
            this.center = new Vector3(0, 0, 0);
            this.eye = new Vector3(0, -150, 0);
            this.target = new Vector3(0, 0, 0);
            this.yaw = (float)Math.PI * 0.75F;
            this.pitch = MathHelper.PiOver4;
            this.distance = 200.0f;

            this.isOrthogonal = false;
        }

        public void init()
        {
            CreateMatrices();
        }

        public void reset()
        {
            this.yaw = (float)Math.PI * 0.75F;
            this.pitch = MathHelper.PiOver4;
            this.distance = 200.0f;
        }


        public Vector3 Position { set { this.eye = value; } get { return this.eye; } }
        public Vector3 UpDirection { set { this.up = value; } get { return this.up; } }
        public Vector3 Center { set { this.center = value; } get { return this.center; } }
        public Vector3 Target { set { this.target = value; } get { return this.target; } }
        public float Yaw { set{ this.yaw = value; } get { return this.yaw; } }
        public float Pitch { set{ this.pitch = value; } get { return this.pitch; } }
        public float Distance { set { this.distance = value; } get { return this.distance; } }
        public bool Ortho { set { this.isOrthogonal = value; } get { return this.isOrthogonal; } }
        public float FOV { set { this.fov = value; } get { return this.fov; } }
        public float NearZ { set { this.zNear = value; } get { return this.zNear; } }
        public float FarZ { set { this.zFar = value; } get { return this.zFar; } }

        public Matrix getView() { return this.view; }
        public Matrix getProjection() { return this.projection; }


        



        public void update(GraphicsDevice graphics, GameTime gameTime, Vector3 target)
        {
            float yawCos = (float)Math.Cos(this.Yaw);
            float yawSin = (float)Math.Sin(this.Yaw);

            float pitchCos = (float)Math.Cos(this.Pitch);
            float pitchSin = (float)Math.Sin(this.Pitch);

            this.target = target;

            this.Position = new Vector3(this.distance * pitchCos * yawSin + target.X,
                                       this.distance * pitchSin + target.Y,
                                       this.distance * pitchCos * yawCos + target.Z);


            //this.Position = new Vector3(this.distance + target.X,
            //                           this.distance + target.Y,
            //                           this.distance + target.Z);

            //this.target = target;
            CreateMatrices();

        }

        private void CreateMatrices()
        {
            CreateViewMatrix();
            CreateProjectionMatrix();
        }

        private void CreateViewMatrix()
        {
            this.view = Matrix.CreateLookAt(this.eye, this.target, this.up);
        }

        private void CreateProjectionMatrix()
        {
            if(this.isOrthogonal == true){
                this.projection = Matrix.CreateOrthographic(graphicsManager.GraphicsDevice.Viewport.AspectRatio * this.distance, this.distance, this.zNear, this.zFar);
            }else{
                this.projection = Matrix.CreatePerspectiveFieldOfView(this.fov,
                graphicsManager.GraphicsDevice.Viewport.AspectRatio, this.zNear, this.zFar);
            }
        }


    }
}
