﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace PlatformerGame
{
    class Player : GameObject, Observer
    {
        #region Fields
        
//        private static Player instance = null;
        private Camera2 camera;
        private Vector3 extents;
        public bool isAlive;
        public bool reachedEndPoint;
        
        
        #endregion

        #region Properties

        #endregion

        override
        public BoundingBox getBoundingBox()
        {
            return new BoundingBox(position-extents, position+extents);     
        }

        public override void createBoundingBox()
        {
            
        }

        override
        public BoundingSphere createBoundingSphere(Vector3 scaling_factor)
        {
            return new BoundingSphere(position, extents.Length());
        }

        #region Constructors
        //Gesture lastGesture;

       
        public Player(Vector3 extents)
        {
            this.extents = extents;
            this.isAlive = false;
            this.reachedEndPoint = false;
        }


        #endregion

        #region Movements

        public void MoveForward()
        {
            Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
            Vector3 v = new Vector3(0, 0, 5);
            v = Vector3.Transform(v, forwardMovement);
            this.position.Z += v.Z;
            this.position.X += v.X;
            this.position.Y += v.Y;
        }

        public void MoveBackward()
        {
            //forward movement with minus move speed = backward movement.
            Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
            Vector3 v = new Vector3(0, 0, -5);
            v = Vector3.Transform(v, forwardMovement);
            this.position.Z += v.Z;
            this.position.X += v.X;
            this.position.Y += v.Y;
        }

        public void TurnLeft()
        {
            Camera2.Instance().TurnLeft();
        }

        public void TurnRight()
        {
            Camera2.Instance().TurnRight();
        }

        public void LookUp()
        {
            Camera2.Instance().LookUp();
        }

        public void LookDown()
        {
            Camera2.Instance().LookDown();
        }

        #endregion

        #region Update

        public void update(GameTime gameTime){
            Camera2.Instance().update(gameTime, Position + Vector3.Up*60);


            if ( this.isAlive && this.getBoundingBox().Min.Y < -13.0f)
            {
                this.isAlive = false;
            }

            if (this.reachedEndPoint == false && this.isAlive && this.position.X < -2400f)
            {
                this.reachedEndPoint = true;
                Console.WriteLine("FINISH THE GAME HERE");
            }
        }

        #endregion


        internal void handleInput(ref Microsoft.Xna.Framework.Input.KeyboardState currentKeyboardState, GameTime gameTime)
        {
            //Debug USE ONLY
            if (currentKeyboardState.IsKeyDown(Keys.Left)) { TurnLeft(); } //Left arrow
            if (currentKeyboardState.IsKeyDown(Keys.Right)) { TurnRight(); } //Right arrow
            if (currentKeyboardState.IsKeyDown(Keys.OemPlus)) { LookUp(); } // + button
            if (currentKeyboardState.IsKeyDown(Keys.OemMinus)) { LookDown(); } // - button
            if (currentKeyboardState.IsKeyDown(Keys.Up)) { MoveForward(); } // up arrow
            if (currentKeyboardState.IsKeyDown(Keys.Down)) { MoveBackward(); } // down arrow
            if (currentKeyboardState.IsKeyDown(Keys.V)) { Console.WriteLine(this.getBoundingBox().Min); }

            //if (currentKeyboardState.IsKeyDown(Keys.Space))
            //{
            //    Console.WriteLine("==========================");
            //    Console.WriteLine("Position :: " + this.Position);
            //    Console.WriteLine("Target :: " + this.Target);
            //    Console.WriteLine("Pitch :: " + this.pitch);
            //    Console.WriteLine("Yaw :: " + this.yaw);
            //}
           
        }
        
        override
        public void draw(Camera2 camera, bool checkBone)
        {

        }
    }

}
