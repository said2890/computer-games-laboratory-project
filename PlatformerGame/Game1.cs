using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using AnimationAux;

namespace PlatformerGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        bool isGameFinished;
        bool usingKinect;
        KinectManager mKinectManager;
        SpeechManager mSpeechManager;
        float[] x;
        float[] y;
        float[] z;
        bool ballcreated;
        AnimationPlayer animPlayer;
        AnimationClip clip;
        GameObject box2;

        float playAnimUntil;

        Player player;
        Camera2 camera;
        Model model;

        #region Constants

        public const int FrustumGroupIndex = 0;
        public const int AABoxGroupIndex = 1;
        public const int OBoxGroupIndex = 2;
        public const int SphereGroupIndex = 3;
        public const int RayGroupIndex = 4;
        public const int NumGroups = 5;

        public const int TriIndex = 0;
        public const int SphereIndex = 1;
        public const int AABoxIndex = 2;
        public const int OBoxIndex = 3;
        public const int NumSecondaryShapes = 4;

        public const float CAMERA_SPACING = 100.0F;

        public const float YAW_RATE = 1;            // radians per second for keyboard controls
        public const float PITCH_RATE = 0.75f;      // radians per second for keyboard controls
        public const float YAW_DRAG_RATE = .01f;     // radians per pixel for drag control
        public const float PITCH_DRAG_RATE = .01f;   // radians per pixel for drag control
        public const float PINCH_ZOOM_RATE = .01f;  // scale factor for pinch-zoom rate
        public const float DISTANCE_RATE = 100;

        public float ScrollVal;

        #endregion

        #region Fields

        Vector3 GRAVITY = new Vector3(0.0f, -9.8f, 0.0f);

        // Rendering helpers
        //GraphicsDeviceManager graphics;
        DebugDraw debugDraw;

        // Secondary shapes.
        Triangle[] secondaryTris = new Triangle[NumGroups];
        BoundingSphere[] secondarySpheres = new BoundingSphere[NumGroups];
        BoundingBox[] secondaryAABoxes = new BoundingBox[NumGroups];
        BoundingOrientedBox[] secondaryOBoxes = new BoundingOrientedBox[NumGroups];

        // Collision results
        ContainmentType[,] collideResults = new ContainmentType[NumGroups, NumSecondaryShapes];


        Model ballModel;
        Model boxModel;

        MouseState currentMouseState = new MouseState();
        MouseState oldMouseState = new MouseState();
        KeyboardState currentKeyboardState = new KeyboardState();

        TimeSpan unpausedClock = new TimeSpan();
        bool paused;

        GameObject ground1, ground2, ground3;
        GameObject barrier1, barrier2;
        GameObject tree1, tree2, tree3, tree4, tree5, tree6;
        GameObject wall, wall2, wall3, wall4,wall5, go;
        GameObject wall21, wall22, wall23, wall24, wall25;
        GameObject wall31, wall32;
        GameObject ball2, ball4, ball5;
        GameObject ball3;
        GameObject box;

        List<GameObject> addToListObjects;
        List<GameObject> objects;
        List<GameObject> staticObjects;
        List<GameObject> visualObjects;

        AnimatedModel animation_model = null;
        AnimatedModel dance = null;
        private bool showDebugInfo = false;

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            isGameFinished = false;
            animation_model = new AnimatedModel("Models\\Victoria-hat-tpose");

            box = new GameObject(); box.n = "BOX1";
            ball2 = new GameObject(); ball2.n = "ball2";
            ball3 = new GameObject(); ball3.n = "ball3";
            ball4 = new GameObject(); ball4.n = "ball4";
            ball5 = new GameObject(); ball5.n = "ball5";

            box2 = new GameObject();

            barrier1 = new GameObject();
            barrier2 = new GameObject();

            tree1 = new GameObject();
            tree2 = new GameObject();
            tree3 = new GameObject();
            tree4 = new GameObject();
            tree5 = new GameObject();
            tree6 = new GameObject();

            ground1 = new GameObject(); ground1.n = "ground1";
            ground3 = new GameObject();
            ground2 = new GameObject();
            
            //ground2 = new Box();
            //ground3 = new Box();
            //ground4 = new Box();

            wall = new GameObject();
            wall2 = new GameObject();
            wall3 = new GameObject();
            wall4 = new GameObject();
            wall5 = new GameObject();

            wall31 = new GameObject();
            wall32 = new GameObject();

            wall21 = new GameObject();
            wall22 = new GameObject();
            wall23 = new GameObject();

            wall24 = new GameObject();
            wall25 = new GameObject();


            go = new GameObject();

            objects = new List<GameObject>();
            staticObjects = new List<GameObject>();
            visualObjects = new List<GameObject>();
            addToListObjects = new List<GameObject>();


            usingKinect = false;

            ballcreated = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.Window.Title = "Platform Game by Group 7";

            animation_model.Position = new Vector3(-3000, 15, 0);
            animation_model.Rotation = new Vector3(0, 90,0);
            animation_model.Scaling = new Vector3(0.7f, 0.7f, 0.7f);

            go.init();
            go.Restitution = 0.2f;
            go.Position = new Vector3(0, 300, 565);
            go.Rotation = Vector3.Zero;
            go.Scaling = new Vector3(0.2f, 0.2f, 0.2f);
            go.Mass = 10.0f;

            box.init();
            box.Restitution = 0.55f;
            box.n = "THE BOX1";
            box.Position = new Vector3(0, 500, 0);
            box.Rotation = Vector3.Zero;
            box.Scaling = new Vector3(0.2f, 0.2f, 0.2f);
            box.Friction = 0.95f;
            box.IsSpherical = false;
            box.Mass = 5.0f;

            ball2.init();
            ball2.Restitution = 0.35f;
            ball2.Position = new Vector3(0, 800, 510);
            ball2.Scaling = new Vector3(30.2f, 30.2f, 30.2f);
            ball2.Rotation = Vector3.Zero;
            ball2.Friction = 0.95f;
            ball2.Mass = 25.0f;
            ball2.IsSpherical = true;

            ball3.init();
            ball3.Restitution = 0.55f;
            ball3.Position = new Vector3(0, 15, 500);
            ball3.Scaling = new Vector3(30.2f, 30.2f, 30.2f);
            ball3.Rotation = Vector3.Zero;
            ball3.Friction = 0.95f;
            ball3.Mass = 5.0f;
            ball3.IsSpherical = true;

            ball4.init();
            ball4.Restitution = 0.35f;
            ball4.Position = new Vector3(0, 800, 565);
            ball4.Scaling = new Vector3(30.2f, 30.2f, 30.2f);
            ball4.Rotation = Vector3.Zero;
            ball4.Friction = 0.95f;
            ball4.Mass = 5.0f;
            ball4.IsSpherical = true;

            ball5.init();
            ball5.Restitution = 0.55f;
            ball5.Position = new Vector3(0, 15, 550);
            ball5.Scaling = new Vector3(30.2f, 30.2f, 30.2f);
            ball5.Rotation = Vector3.Zero;
            ball5.Friction = 0.95f;
            ball5.Mass = 5.0f;
            ball5.IsSpherical = true;

            barrier1.init();
            barrier1.Restitution = 0f;
            barrier1.Position = new Vector3(-100, 30, 100);
            barrier1.Rotation = new Vector3(90, 0, 90);
            barrier1.IsStatic = true;
            barrier1.Scaling = new Vector3(1, 1, 1);
            barrier1.Mass = 500.0f;
            
            barrier2.init();
            barrier2.Restitution = 0f;
            barrier2.Position = new Vector3(-400, 30, 400);
            barrier2.Rotation = new Vector3(0, 0, 90);
            barrier2.Scaling = new Vector3(1, 1, 1);
            barrier2.IsStatic = true;
            barrier2.Mass = 500.0f;

            tree1.init();
            tree1.Position = new Vector3(-800, 0, 700);
            tree1.Rotation = new Vector3(-90, 0, 0);
            tree1.IsStatic = true;
            tree1.Mass = 500.0f;

            tree2.init();
            tree2.Position = new Vector3(-500, 0, 0);
            tree2.Rotation = new Vector3(-90, 0, 0);
            tree2.Scaling = new Vector3(8f, 8f, 8f);
            tree2.IsStatic = true;
            tree2.Mass = 500.0f;

            tree3.init();
            tree3.Position = new Vector3(1790, 0, -1850);
            tree3.Rotation = new Vector3(-90, 0, 0);
            tree3.Scaling = new Vector3(30.0f, 30.0f, 30.0f);
            tree3.IsStatic = true;
            tree3.Mass = 500.0f;

            tree4.Position = new Vector3(-1460, 0, 1670);
            tree4.Rotation = new Vector3(-90, 40, 0);
            tree4.Scaling = new Vector3(30.6f, 19f, 20.6f);
            tree4.IsStatic = true;
            tree4.Mass = 500.0f;

            tree5.Position = new Vector3(-1550, 0, 1170);
            tree5.Rotation = new Vector3(-90, 0, 0);
            tree5.Scaling = new Vector3(30.6f, 25f, 30.4f);
            tree5.IsStatic = true;
            tree5.Mass = 500.0f;

            tree6.Position = new Vector3(1450f, 0, 560f);
            tree6.Rotation = new Vector3(-90, 0, 0);
            tree6.Scaling = new Vector3(50f, 70.5f, 100f);
            tree6.IsStatic = true;
            tree6.Mass = 500.0f;

            //go.init();
            //go.Position = new Vector3(0, 0, 0);
            //go.Rotation = Vector3.Zero;
            //go.Scaling = new Vector3(1f, 1f, 1f);
            ////box2.init();
            //box2.Position = new Vector3(0, 30, -200.0f);
            //box2.Scaling = 0.05f;
            //Console.WriteLine(box2.Position);

            ground1.init();
            ground1.Friction = 0.9f;
            ground1.Position = new Vector3(0, 0, 0);
            ground1.Rotation = new Vector3(90, 0, 0);
            ground1.Scaling = new Vector3(5.0f, 1.0f, 5.0f);
            ground1.Restitution = 1f;
            ground1.Mass = 100000f;
            ground1.IsStatic = true;

            ground2.init();
            ground2.Friction = 0.9f;
            ground2.Position = new Vector3(-3540, 0, 0);
            ground2.Rotation = new Vector3(90, 0, 0);
            ground2.Scaling = new Vector3(3.0f, 1.0f, 3.0f);
            ground2.Restitution = 1f;
            ground2.Mass = 100000f;
            ground2.IsStatic = true;

            ground3.init();
            ground3.Position = new Vector3(-2000, -40, 0);
            ground3.Rotation = new Vector3(90, 0, 0);
            ground3.Scaling = new Vector3(2.0f, 1.0f, 2.0f);
            ground3.Friction = 0.9f;
            ground3.Restitution = 1f;
            ground3.Mass = 100000f;
            ground3.IsStatic = true;

            //ground4.init();
            //ground4.Position = new Vector3(28, 0, 685);
            //ground4.Rotation = new Vector3(90, 0, 0);
            //ground4.Scaling = new Vector3(1.0f, 1.0f, 1.0f);

            wall.init();
            wall.Position = new Vector3(1970, 100, 0);
            wall.Rotation = new Vector3(90, 0, 0);
            wall.Scaling = new Vector3(1, 1, 5);
            wall.IsStatic = true;
            wall.Mass = 5000f;

            wall2.init();
            wall2.Position = new Vector3(0, 100, 1970);
            wall2.Rotation = new Vector3(90, 90, 0);
            wall2.Scaling = new Vector3(5, 1, 1);
            wall2.IsStatic = true;
            wall2.Mass = 5000f;

            wall3.init();
            wall3.Position = new Vector3(-1970, 100, -1060);
            wall3.Rotation = new Vector3(90, 0, 0);
            wall3.Scaling = new Vector3(1, 1, 2.3f);
            wall3.IsStatic = true;
            wall3.Mass = 5000f;
            
            wall5.init();
            wall5.Position = new Vector3(-1970f, 100, 1060);
            wall5.Rotation = new Vector3(90, 0, 0);
            wall5.Scaling =  new Vector3(1, 1, 2.3f);
            wall5.IsStatic = true;
            wall5.Mass = 5000f;

            wall4.init();
            wall4.Position = new Vector3(0, 100, -1970);
            wall4.Rotation = new Vector3(90, -90, 0);
            wall4.Scaling = new Vector3(5, 1, 1);
            wall4.IsStatic = true;
            wall4.Mass = 5000f;

            wall31.init();
            wall31.Position = new Vector3(-2165f, 100, 157);
            wall31.Rotation = new Vector3(90, -90, 0);
            wall31.Scaling = new Vector3(0.5f, 1, 1);
            wall31.IsStatic = true;
            wall31.Mass = 5000f;
            
            wall32.init();
            wall32.Position = new Vector3(-2165f, 100, -157);
            wall32.Rotation = new Vector3(90, -90, 0);
            wall32.Scaling = new Vector3(0.5f, 1, 1);
            wall32.IsStatic = true;
            wall32.Mass = 5000f;

            wall21.init();
            wall21.Position = new Vector3(-3540, 100, 1180);
            wall21.Rotation = new Vector3(90, 90, 0);
            wall21.Scaling = new Vector3(3f, 1, 1);
            wall21.IsStatic = true;
            wall21.Mass = 5000f;

            wall22.init();
            wall22.Position = new Vector3(-4725, 100, 0);
            wall22.Rotation = new Vector3(90, 0, 0);
            wall22.Scaling = new Vector3(1, 1, 3f);
            wall22.IsStatic = true;
            wall22.Mass = 5000f;

            wall23.init();
            wall23.Position = new Vector3(-3540, 100, -1180);
            wall23.Rotation = new Vector3(90, -90, 0);
            wall23.Scaling = new Vector3(3, 1, 1);
            wall23.IsStatic = true;
            wall23.Mass = 5000f;

            wall24.init();
            wall24.Position = new Vector3(-2365, 100, -665);
            wall24.Rotation = new Vector3(90, 0, 0);
            wall24.Scaling = new Vector3(1, 1, 1.3f);
            wall24.IsStatic = true;
            wall24.Mass = 5000f;

            wall25.init();
            wall25.Position = new Vector3(-2365, 100, 665);
            wall25.Rotation = new Vector3(90, 0, 0);
            wall25.Scaling = new Vector3(1, 1, 1.3f);
            wall25.IsStatic = true;
            wall25.Mass = 5000f;

            player = new Player(new Vector3(30f, 30f, 30f));
            player.Position = new Vector3(-1000, 100, 0);
            player.Mass = 70.0f;
            player.IsSpherical = true;

            //wall5.init();
            //wall5.Position = new Vector3(25, 0, -290);
            //wall5.Rotation = new Vector3(0, 90, 90);
            //wall5.Scaling = new Vector3(1.0f, 1.0f, 1.0f);

            //wall6.init();
            //wall6.Position = new Vector3(-750, 0, -290);
            //wall6.Rotation = new Vector3(0, 90, 90);
            //wall6.Scaling = new Vector3(1.0f, 1.0f, 1.0f);

            //wall7.init();
            //wall7.Position = new Vector3(420,115,100);
            //wall7.Rotation = new Vector3(90, 0, 0);
            //wall7.Scaling = new Vector3(1.0f, 1.0f, 1.0f);

            //wall8.init();
            //wall8.Position = new Vector3(420, 115, 685);
            //wall8.Rotation = new Vector3(90, 0, 0);
            //wall8.Scaling = new Vector3(1.0f, 1.0f, 1.0f);

            staticObjects.Add(ground1);
            staticObjects.Add(ground2);
            staticObjects.Add(ground3);
            staticObjects.Add(wall);
            staticObjects.Add(wall2);
            staticObjects.Add(wall3);
            staticObjects.Add(wall4);
            staticObjects.Add(wall5);
            staticObjects.Add(wall31);
            staticObjects.Add(wall32);
            staticObjects.Add(wall21);
            staticObjects.Add(wall22);
            staticObjects.Add(wall23);
            staticObjects.Add(wall24);
            staticObjects.Add(wall25);
            staticObjects.Add(barrier1);
            staticObjects.Add(barrier2);
            staticObjects.Add(tree1);
            staticObjects.Add(tree2);
            staticObjects.Add(tree3);
            staticObjects.Add(tree4);
            staticObjects.Add(tree5);
            staticObjects.Add(tree6);

           // objects.Add(go);
            objects.Add(box);
            objects.Add(ball2);
            objects.Add(ball3);
            objects.Add(ball4);
            objects.Add(ball5);
            objects.Add(player);

           
            // foreach(DisplayMode dm in GraphicsAdapter.DefaultAdapter.SupportedDisplayModes){
            //     Console.WriteLine(dm.Width + "  " + dm.Height + " - " + dm.AspectRatio);
            //  }

          

            mKinectManager = KinectManager.getInstance(this);
            mSpeechManager = SpeechManager.getInstance(ref mKinectManager);
            mKinectManager.setPlayerRef(ref player);
            graphics.PreferredBackBufferHeight = 480;
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferMultiSampling = false;
            //Console.WriteLine(graphics.GraphicsDevice.PresentationParameters.Bounds.Width);
            //Console.WriteLine(graphics.GraphicsDevice.PresentationParameters.Bounds.Height);
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            camera = Camera2.Instance();
            camera.AspectRatio = graphics.GraphicsDevice.DisplayMode.AspectRatio;
            camera.Position = new Vector3(-1000, 100, 0);
            //camera.Target = new Vector3(0, 0, 0);
            camera.Pitch = 0.2f;
            camera.makeReady();

            KinectBody.getInstance().setGameWindowSize(this.Window.ClientBounds.Width, this.Window.ClientBounds.Height);
            
            String err = mKinectManager.InitKinect();
            if (err != "" && usingKinect == true)
            {
                Console.WriteLine(err);
                Exit();


            }
  

            Console.WriteLine("DEBUG - Game Initialize!");

            debugDraw = new DebugDraw(GraphicsDevice);


            //Content.RootDirectory = "Content";

            //Components.Add(new FrameRateCounter(this));

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            if(usingKinect)
            mKinectManager.loadKinectResources(GraphicsDevice);

            model = Content.Load<Model>("Models\\ground2");
            //model = Content.Load<Model>("moxi\\apple");
            
            ground2.loadModel(model);
            ground1.loadModel(model);

            model = Content.Load<Model>("Models\\lava");
            ground3.loadModel(model);
            // ground2.loadModel(model);
          //  ground3.loadModel(model);
          //  ground4.loadModel(model);

            model = Content.Load<Model>("Models\\wall2");
            wall.loadModel(model);
            wall2.loadModel(model);
            wall3.loadModel(model);
            wall4.loadModel(model);
            wall5.loadModel(model);
            wall31.loadModel(model);
            wall32.loadModel(model);

            wall21.loadModel(model);
            wall22.loadModel(model);
            wall23.loadModel(model);
            wall24.loadModel(model);
            wall25.loadModel(model);

            boxModel = Content.Load<Model>("Models\\Box");
            box.loadModel(boxModel);
            go.loadModel(boxModel);

            ballModel = Content.Load<Model>("Models\\ball3");
            ball2.loadModel(ballModel);
            ball3.loadModel(ballModel);
            ball4.loadModel(ballModel);
            ball5.loadModel(ballModel);

            model = Content.Load<Model>("Models\\barrier2");
            barrier1.loadModel(model);
            barrier2.loadModel(model);

            model = Content.Load<Model>("Models\\treeone2");
            tree1.loadModel(model);

            model = Content.Load<Model>("Models\\treetwo2");
            tree2.loadModel(model);
            tree3.loadModel(model);
            tree4.loadModel(model);
            tree5.loadModel(model);
            tree6.loadModel(model);

            
            animation_model.LoadContent(Content);

            dance = new AnimatedModel("Models\\Victoria-hat-dance");
            dance.LoadContent(Content);

            clip = dance.Clips[0];
            //animPlayer = animation_model.Clips = 
            animPlayer = animation_model.PlayClip(clip);

           // And play the clip
          // animPlayer = animation_model.PlayClip(clip);
           // player.Looping = true;
           // model = Content.Load<Model>("moxi\\player");

            foreach (GameObject obj in objects)
            {
                obj.createBoundingBox();
            }

            foreach (GameObject obj in staticObjects)
            {
                obj.createBoundingBox();
            }

           // go.loadModel(model);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            
           // mKinectManager.DisposeResources();
            mSpeechManager.Dispose();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // Allows the game to exit
            currentKeyboardState = Keyboard.GetState();
            currentMouseState = Mouse.GetState();

            if (currentKeyboardState.IsKeyDown(Keys.Escape) || isGameFinished)
                this.Exit();

           

            if (Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.D))
                showDebugInfo = !showDebugInfo;

            if (currentMouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton != ButtonState.Pressed) createBoxFromDrawing(); 
            if (currentMouseState.RightButton == ButtonState.Pressed && oldMouseState.RightButton != ButtonState.Pressed) createBallFromDrawing();
            //HandleInput(gameTime);
            //camera.update(graphics.GraphicsDevice, gameTime, new Vector3(0, 0, 0));//new Vector3(0,0, 0)

            animation_model.Update(gameTime);

            player.handleInput(ref currentKeyboardState, gameTime);
            player.update(gameTime);//new Vector3(0,0, 0)

           
          //  box.update(graphics.GraphicsDevice, gameTime);

            //ball2.Acceleration = new Vector3(0.0f, -98f, 0.0f);
          //  ball2.update(graphics.GraphicsDevice, gameTime);

            //ball4.Acceleration = new Vector3(0.0f, -98f, 0.0f);
          //  ball4.update(graphics.GraphicsDevice, gameTime);

           /// ball3.Acceleration = new Vector3(0.0f, -98f, 0.0f);
           // ball3.update(graphics.GraphicsDevice, gameTime);

           // ball5.Acceleration = new Vector3(0.0f, -98f, 0.0f);
           // ball5.update(graphics.GraphicsDevice, gameTime);

         

            for (int i = 0; i < objects.Count; i++)
            {
                objects.ElementAt(i).update(graphics.GraphicsDevice, gameTime); 

            }

            for (int i = 0; i < addToListObjects.Count; i++)
            {
                addToListObjects.ElementAt(i).update(graphics.GraphicsDevice, gameTime);

            }

            for (int i = 0; i < objects.Count; i++)
            {
                GameObject currentObject = objects.ElementAt(i);
                for (int j = i + 1; j < objects.Count; j++)
                {
                    GameObject nextObject = objects.ElementAt(j);
                    //if(currentObject.createBoundingSphere(currentObject.Scaling).Intersects(nextObject.createBoundingSphere(nextObject.Scaling)))
                    CollisionDetector.Instance.Detect(ref currentObject,ref nextObject);
                }

                //for (int l = 0; l < addToListObjects.Count; l++)
                //{
                //    GameObject createdObject = addToListObjects.ElementAt(l);
                //    CollisionDetector.Instance.Detect(ref currentObject, ref createdObject);

                //}

                for (int k = 0; k < staticObjects.Count; k++)
                {
                    GameObject nextstaticObject = staticObjects.ElementAt(k);
                    CollisionDetector.Instance.Detect(ref currentObject, ref nextstaticObject);
                }
            }

            for (int i = 0; i < addToListObjects.Count; i++)
            {
                GameObject addedObject = addToListObjects.ElementAt(i);
                CollisionDetector.Instance.Detect(ref addedObject, ref ground1);
            }

            //int c = 0;
            //for (int i = 0; i < addToListObjects.Count; i++)
            //{
            //    GameObject toBeAdded = addToListObjects.ElementAt(i);
            //    this.objects.Add(toBeAdded);
            //    c++;
            //}

            //if (c > 0)
            //{
            //    addToListObjects = new List<GameObject>();
            //}


            if (usingKinect) //usingKinect
            {
                mKinectManager.update(gameTime);

            }


            if (player.isAlive == false)
            {
                resetPlayer();
                player.isAlive = true;
            }

            if (player.reachedEndPoint == true)
            {
                finishTheGame(gameTime);
                //isGameFinished = true;
            }
    

            if (!paused)
            {
                unpausedClock += gameTime.ElapsedGameTime;
            }


           // lastHeadPos = curHeadPos;
            oldMouseState = currentMouseState;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            DepthStencilState newState = new DepthStencilState();
            newState.DepthBufferEnable = true;
            GraphicsDevice.DepthStencilState = newState;
           

            
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ball2);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ball3);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ball4);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ball5);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, barrier1);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, barrier2);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, tree1);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, tree2);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ground1);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ground2);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ground3);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, ground4);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall2);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall3);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall4);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall5);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall6);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall7);
            //CollisionDetector.Instance.DebugRenderBox(debugDraw, wall8);


            //CollisionDetector.Instance.DebugRenderSphereAndSphere(debugDraw);
            // CollisionDetector.Instance.Detect(

         


            //wall.draw(this.camera, false);
            //wall2.draw(this.camera, false);
            //wall3.draw(this.camera, false);
            //wall4.draw(this.camera, false);
            ////wall5.draw(this.camera, false);
            ////wall6.draw(this.camera, false);
            ////wall7.draw(this.camera, false);
            ////wall8.draw(this.camera, false);


            for (int i = 0; i < staticObjects.Count; i++)
            {
                staticObjects.ElementAt(i).draw(this.camera, false);
            }
            //ground1.draw(this.camera, false);
            //ground2.draw(this.camera, false);
            //ground3.draw(this.camera, false);
            //ground4.draw(this.camera, false);

            //box.draw(this.camera, false);
            //ball2.draw(this.camera, false);
            //ball3.draw(this.camera, false);
            //ball4.draw(this.camera, false);
            //ball5.draw(this.camera, false);

            // //box.draw(camera);
            if (showDebugInfo)
            {
                debugDraw.Begin(this.camera.getView(), this.camera.getProjection());
                // debugDraw.DrawWireFrustum(primaryFrustum, Color.Black);
                for (int i = 0; i < objects.Count; i++)
                {
                    if (objects[i].IsSpherical) CollisionDetector.Instance.DebugRenderSphere(debugDraw, objects[i]);
                    else CollisionDetector.Instance.DebugRenderBox(debugDraw, objects[i]);
                }

                for (int i = 0; i < staticObjects.Count; i++)
                {
                    if (staticObjects[i].IsSpherical) CollisionDetector.Instance.DebugRenderSphere(debugDraw, staticObjects[i]);
                    else CollisionDetector.Instance.DebugRenderBox(debugDraw, staticObjects[i]);
                }
                debugDraw.End();
            }

            for (int i = 0; i < objects.Count; i++)
            {
                objects.ElementAt(i).draw(this.camera, false);
            }

           // barrier1.draw(this.camera, false);
          //  barrier2.draw(this.camera, false);

            //tree1.draw(this.camera, false);
            //tree2.draw(this.camera, false);
            //tree3.draw(this.camera, false);
            //tree4.draw(this.camera, false);
            //tree5.draw(this.camera, false);
            //tree6.draw(this.camera, false);


            for (int i = 0; i < addToListObjects.Count; i++)
            {
                addToListObjects.ElementAt(i).draw(this.camera, false);
            }

            animation_model.Draw(this.camera, false);

            if (usingKinect)
            {
                spriteBatch.Begin();
                mKinectManager.draw(spriteBatch);
                spriteBatch.End();
            }


            base.Draw(gameTime);
        }

        public void CheckMouseClick()
        {
            MouseState mouseState = Mouse.GetState();
            if (true)
            // if (mouseState.LeftButton == ButtonState.Pressed)
            {

                Ray pickRay = GetPickRay();
                //Ray pickRay = new Ray(camera.Position, Vector3.Normalize(box.Position - camera.Position));

                //@TODO FIX ME

                //int selectedIndex = -1;
                float selectedDistance = float.MaxValue;
                //for (int i = 0; i < worldObjects.Length; i++)
                //{
                //    worldObjects[i].texture2D = sphereTexture;
                //    BoundingSphere sphere =
                //        worldObjects[i].model.Meshes[0].BoundingSphere;
                //    sphere.Center = worldObjects[i].position;
                //    Nullable<float> result = pickRay.Intersects(sphere);
                //    if (result.HasValue == true)
                //    {
                //        if (result.Value < selectedDistance)
                //        {
                //            selectedIndex = i;
                //            selectedDistance = result.Value;
                //        }
                //    }
                //}
                //if (selectedIndex > -1)
                //{
                //    worldObjects[selectedIndex].texture2D = selectedTexture;
                //}

                // for (int i = 0; i < objects.Count; i++)
                //   {
                //     GameObject go = objects.ElementAt(i);
                //      BoundingSphere sphere = go.createBoundingSphere(go.Scaling.X);//go.Model.Meshes[0].BoundingSphere;

                //     sphere.Center = go.Position;

                for (int i = 0; i < objects.Count; i++)
                {
                    GameObject curObject = objects.ElementAt(i);
                    BoundingSphere sphere = curObject.createBoundingSphere(curObject.Scaling);//go.Model.Meshes[0].BoundingSphere;
               // BoundingSphere sphere = box.createBoundingSphere(box.Scaling);
                    sphere.Center = curObject.Position;

                    Nullable<float> result = pickRay.Intersects(sphere);
                    if (result.HasValue == true)
                    {
                        if (result.Value < selectedDistance)
                        {
                            selectedDistance = result.Value;
                            Console.WriteLine("SELECTED at : " + selectedDistance);


                            ////box.Scaling += new Vector3(0, -1, 0);
                            //if (lastSelected != -1) ;
                            //objects.ElementAt(lastSelected).IsSelected = false;
                            //go.IsSelected = true;
                            //Console.WriteLine("Last was : " + lastSelected);
                            //Console.WriteLine("New is  : " + i);
                            //lastSelected = i;
                            curObject.IsSelected = true;
                            mKinectManager.setSelectedObject(ref curObject);
                            return;
                        }

                        //  }
                        //  Console.WriteLine("no");
                    }
                }





            }


        }
        Ray GetPickRay()
        {
          //  MouseState mouseState = Mouse.GetState();

            //  float mouseX = (float)mouseState.X;
            //  float mouseY = (float)mouseState.Y;
            //float mouseX = mKinectManager.getRightHandPos2D().X; //Console.WriteLine("Kinect point : " + mKinectManager.getRightHandPos2D());
            //  float mouseY = mKinectManager.getRightHandPos2D().Y;// Console.WriteLine("Mouse point : " + mouseState.X + "  " + mouseState.Y);
            float mouseX = KinectBody.getInstance().RightHandPosition2D.X;
            float mouseY = KinectBody.getInstance().RightHandPosition2D.Y;

            Vector3 nearsource = new Vector3(mouseX, mouseY, 0f);
            Vector3 farsource = new Vector3(mouseX, mouseY, 1f);

            Matrix world = Matrix.CreateTranslation(0, 0, 0);

            Vector3 nearPoint = GraphicsDevice.Viewport.Unproject(nearsource,
                this.camera.getProjection(), this.camera.getView(), world);

            Vector3 farPoint = GraphicsDevice.Viewport.Unproject(farsource,
                this.camera.getProjection(), this.camera.getView(), world);

            // Create a ray from the near clip plane to the far clip plane.
            Vector3 direction = farPoint - nearPoint;
            direction.Normalize();
            Ray pickRay = new Ray(nearPoint, direction);

            return pickRay;
        }

        public void createBoxFromDrawing()
        {
            //if (ballcreated == true) return;
            GameObject b = new GameObject();
            b.init();
            Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
            Vector3 v = new Vector3(0, 0, 100f);
            v = Vector3.Transform(v, forwardMovement);

            b.Position += (Camera2.Instance().Position + v + new Vector3(0, 150, 0));


            b.Restitution = 0.55f;
            // b.Position = new Vector3(-0, 900, 0);//new Vector3(0, 520, 0);
            b.Rotation = Vector3.Zero;
            b.Scaling = new Vector3(0.2f, 0.2f, 0.2f);
            b.Friction = 0.95f;
            b.IsSpherical = false;
            b.Mass = 5.0f;

            Model model = boxModel;

            b.loadModel(model);
            b.createBoundingBox();
            b.n = "THE BOX";
            this.objects.Add(b);
            Console.WriteLine(addToListObjects.Count);
            this.ballcreated = true;
        }

        public void createBallFromDrawing()
        {
           // if (this.ballcreated == true) return;
            GameObject b = new GameObject();
            b.init();

            Matrix forwardMovement = Matrix.CreateRotationY(Camera2.Instance().Yaw);
            Vector3 v = new Vector3(0, 0, 100f);
            v = Vector3.Transform(v, forwardMovement);

            b.Position += (Camera2.Instance().Position + v + new Vector3(0, 150, 0));

            b.Restitution = 0.35f;
            // b.Position = new Vector3(-0, 900, 550);
            b.Scaling = new Vector3(30.2f, 30.2f, 30.2f);
            b.Rotation = Vector3.Zero;
            b.Friction = 0.95f;
            b.Mass = 5.0f;
            b.IsSpherical = true;

            Model model = ballModel;
            
            b.loadModel(model);

            b.createBoundingBox();
            this.objects.Add(b);
            this.ballcreated = true;



        }

        void resetPlayer(){
            player.Position = new Vector3(-1000, 100, 0);
        }

        void finishTheGame(GameTime gameTime)
        {
            
             animPlayer.Looping = true ;
            //playAnimUntil += gameTime.ElapsedGameTime.Milliseconds;
            
            
            
        }
     


    }
}
