﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformerGame
{
    interface Observer
    {
        void checkNotif(String val);
    }
}
