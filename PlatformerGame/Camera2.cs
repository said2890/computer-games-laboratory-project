﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformerGame
{
    class Camera2
    {
        private static Camera2 instance = null;
        Matrix projection;
        Matrix view;

        Vector3 target;
        Vector3 up;
        Vector3 center;
        Vector3 eye;
        Vector3 reference;

        float fov;
        float zNear;
        float zFar;

        float yaw;
        float pitch;
        float distance;

        float rotationSpeed;
        float moveSpeed;
        float aspectRatio;

        const float MAX_PITCH_VALUE = 0.50f;

        public static Camera2 Instance(){
            if (instance == null)
            {
                instance = new Camera2();
            }

            return instance;
        }

        public Vector3 Position { set { this.eye = value; } get { return this.eye; } }
        public Vector3 UpDirection { set { this.up = value; } get { return this.up; } }
        public Vector3 Center { set { this.center = value; } get { return this.center; } }
        public Vector3 Target { set { this.target = value; } get { return this.target; } }
        public float Yaw { set { this.yaw = value; } get { return this.yaw; } }
        public float Pitch { set { this.pitch = value; } get { return this.pitch; } }
        public float Distance { set { this.distance = value; } get { return this.distance; } }
        public float AspectRatio { set { this.aspectRatio = value; } get { return this.aspectRatio; } }
        public float FOV { set { this.fov = value; } get { return this.fov; } }
        public float NearZ { set { this.zNear = value; } get { return this.zNear; } }
        public float FarZ { set { this.zFar = value; } get { return this.zFar; } }

        public Matrix getView() { return this.view; }
        public Matrix getProjection() { return this.projection; }

        private Camera2()
        {
           
            this.reference = new Vector3(0, 0, 10);
            this.rotationSpeed = 1f / 60f;
            this.moveSpeed = 180f / 60f;

            this.fov = MathHelper.PiOver4;
            this.up = new Vector3(0.0f, 1.0f, 0.0f);
            this.center = Vector3.Zero;
            this.target = Vector3.Zero;
            this.eye = new Vector3(0, 10, 0);

            this.yaw = 1.5f;
            this.pitch = 0;

            this.aspectRatio = 800 / 640;
            

            this.zNear = 5.0f;
            this.zFar = 10000.0f;

            
        }

        public void makeReady()
        {
            CreateProjectionMatrix();
        }


        public void update(GameTime gameTime, Vector3 actorPos)
        {

            // Calculate the camera's current position.
            this.eye = actorPos;

            // Copy the camera's reference vector.
            this.target = this.reference;

            // Create a vector pointing the direction the camera is facing.
            this.target = Vector3.Transform(this.target,
               Matrix.CreateRotationX(this.pitch)*Matrix.CreateRotationY(this.yaw));

            // Calculate the position the camera is looking at.
            this.target += this.eye;

            // Set up the view matrix and projection matrix.

            CreateViewMatrix();

            if (this.eye.Y < 10)
            {
                this.eye.Y = 10.0f;
            }
        }

        private void CreateMatrices()
        {
            CreateViewMatrix();
            CreateProjectionMatrix();
        }

        private void CreateViewMatrix()
        {

            this.view = Matrix.CreateLookAt(this.eye, this.target,this.up);
        }

        private void CreateProjectionMatrix()
        {
                this.projection = Matrix.CreatePerspectiveFieldOfView(this.fov,
                this.aspectRatio, this.zNear, this.zFar);
        }

        public void LookUp()
        {
            this.pitch -= this.rotationSpeed;
        //limitting the pitch value to prevent flipping the orientation .
            if (this.pitch < -MAX_PITCH_VALUE)
            {
                this.pitch = -MAX_PITCH_VALUE;
            }
        }

        public void LookDown()
        {
            this.pitch += this.rotationSpeed;
            //limitting the pitch value to prevent flipping the orientation .
            if (this.pitch > MAX_PITCH_VALUE)
            {
                this.pitch = MAX_PITCH_VALUE;
            }
        }

        public void TurnLeft()
        {
            // Rotate left.
            this.yaw += this.rotationSpeed;
        }

        public void TurnRight()
        {
            // Rotate right.
            this.yaw -= this.rotationSpeed;
        }

        public void MoveForward()
        {
            Matrix forwardMovement =  Matrix.CreateRotationY(this.yaw);
            Vector3 v = new Vector3(0, 0, this.moveSpeed);
            v = Vector3.Transform(v, forwardMovement);
            this.eye.Z += v.Z;
            this.eye.X += v.X;
            this.eye.Y += v.Y;
        }

        public void MoveBackward()
        {
            //forward movement with minus move speed = backward movement.
            Matrix forwardMovement =  Matrix.CreateRotationY(this.yaw);
            Vector3 v = new Vector3(0, 0, -this.moveSpeed);
            v = Vector3.Transform(v, forwardMovement);
            this.eye.Z += v.Z;
            this.eye.X += v.X;
            this.eye.Y += v.Y;
        }

        public void handleInput(ref KeyboardState keyboardState, GameTime gameTime)
        {
            //Debug USE ONLY
            if (keyboardState.IsKeyDown(Keys.Left)) { TurnLeft(); } //Left arrow
            if (keyboardState.IsKeyDown(Keys.Right)) { TurnRight(); } //Right arrow
            if (keyboardState.IsKeyDown(Keys.OemPlus)) { LookUp(); } // + button
            if (keyboardState.IsKeyDown(Keys.OemMinus)) { LookDown(); } // - button
            if (keyboardState.IsKeyDown(Keys.Up)) { MoveForward(); } // up arrow
            if (keyboardState.IsKeyDown(Keys.Down)) { MoveBackward(); } // down arrow
    
            if (keyboardState.IsKeyDown(Keys.Space))
            {
                Console.WriteLine("==========================");
                Console.WriteLine("Position :: " + this.Position);
                Console.WriteLine("Target :: " + this.Target);
                Console.WriteLine("Pitch :: " + this.pitch);
                Console.WriteLine("Yaw :: " + this.yaw);
            }
           
          
        }

        
    }
}
