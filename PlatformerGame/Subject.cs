﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlatformerGame
{
    interface Subject
    {
        void attach(Observer o);
        void detach(Observer o);
        void notify();
    }
}
